//class Server
#include "Server.h"

Server::Server()
{
    sock_fd = 0;
    epoll_fd = 0;
    serverAddr.sin_family = PF_INET;
    serverAddr.sin_port = htons(SERVER_PORT);
    //serverAddr.sin_addr.s_addr = inet_addr(SERVER_HOST);
    serverAddr.sin_addr.s_addr = INADDR_ANY;
}

void Server::Init()
{
    
    cout<<"Init Server..."<<endl;
    sock_fd = socket(PF_INET, SOCK_STREAM, 0);
    if( sock_fd < 0 ){
        perror("#socket error");
        exit(-1);
    }
    
    if(bind(sock_fd, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0){
        perror("#bind error");
        exit(-1);
    }
    
    int ret = listen(sock_fd,5);
    if(ret < 0){
        perror("listen error");
        exit(-1);
    }
    
    cout<<"Start to listen Server Host: "<< inet_ntoa(serverAddr.sin_addr)<<":"<<ntohs(serverAddr.sin_port)<<endl;
    //printf("Start to listen: %s\n", SERVER_HOST );
    
    epoll_fd = epoll_create(EPOLL_SIZE);
    if(epoll_fd < 0) {
        perror("epoll_fd error");
        exit(-1);
    }

    addfd( epoll_fd, sock_fd, true );   
}

void Server::Start()
{
    static struct epoll_event ev[MAXEVENTNUM];
    Init();

    while(1){
        //cout<<"epoll_wait:"<<endl;
        int events_count = epoll_wait(epoll_fd, ev, MAXEVENTNUM, -1 );
        if(events_count < 0){
            perror("epoll_wait error:");
            break;
        }
        //cout<<"epoll events count = " <<events_count<<endl;
        for(int i =0; i<events_count; i++){
            if( ev[i].data.fd == sock_fd ){             
                struct sockaddr_in clientAddr;
                socklen_t clientAddrLength = sizeof(struct sockaddr_in);
                int client_fd = accept(sock_fd, (struct sockaddr *)&clientAddr, &clientAddrLength);
                
                if( client_fd==-1 ){
                    perror("accept error");
                    continue;
                }else{                         
                    cout<<"\nConnected from IP: "
                    <<inet_ntoa(clientAddr.sin_addr)
                    <<" Port: "     <<ntohs(clientAddr.sin_port)
                    <<" clientfd: " <<client_fd<<endl;
                    
                    addfd(epoll_fd, client_fd, true);
                    clientfdList.push_back(client_fd);
                    cout<<"There are "<<clientfdList.size()<<" clients in the chat room"<<endl;
                    
                    //cout<<"send message to new client:welcome to joint chartroom."<<endl;
                    char message[] = WELCOMEMESG;                          
                    int ret = send(client_fd,message,strlen(message),0);
                    if(ret < 0){
                        perror("send message error");
                        Close();
                        exit(-1);
                    }
                    /*
                    #ifdef TESTFOR_BUFF
                        char buffTest[MAXBUFF];
                        int rett = recv(client_fd, buffTest, MAXBUFF, 0);
                        if(rett<0){
                            perror("buffer test error");
                        }
                        else{
                            buffTest[rett] = '\0';
                            cout<<rett<<" buffer:"<<buffTest<<endl;
                        }
                    #endif
                    */
                }
            }else{  
                char buffRecv[MAXBUFF];
                bzero(buffRecv,MAXBUFF);
 
                int n = recv(ev[i].data.fd, buffRecv, MAXBUFF, 0);
                if(n == 0){     
                    close(ev[i].data.fd);
                    clientfdList.remove(ev[i].data.fd);
                    cout<<"\nclientID "<<ev[i].data.fd
                    <<" exit, remain "<<clientfdList.size()<<" clients in the chart room"<<endl;
                }
                else{
                    buffRecv[n-1] = '\0';
                    cout<<"# clientID "<<ev[i].data.fd<<": "<<buffRecv<<endl;
                    
                    if( SentBoardcastMessage(ev[i].data.fd, buffRecv) < 0 ){
                        perror("SentBoardcastMessage error:");
                        Close();
                        exit(-1);
                    }
                }
            }
        }
    }
    Close();
}

int Server::SentBoardcastMessage(int connect_fd, char buff[])
{
    //char strBoard[] = "(Boardcast Message)";
    char buffSend[MAXBUFF];
    bzero(buffSend,MAXBUFF);
    //strcat(buffSend,buff);
    //strcat(buffSend,strBoard);
    sprintf(buffSend,"# clientID %d: %s",connect_fd,buff);
    //cout<<"#Boardcast Message :"<<buffSend<<endl;
    for(list<int>::iterator it = clientfdList.begin(); it!=clientfdList.end(); it++){
        if(*it != connect_fd)
            if( send(*it, buffSend, MAXBUFF, 0) < 0){
                perror("Boardcast message error");
                return -1;
            }
    }
    return 0;
}

void Server::Close(){
    close(epoll_fd);
    close(sock_fd);
}



































