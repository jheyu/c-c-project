//Client.h 
#ifndef CLIENT_H
#define CLIENT_H
#include "Common.h"

class Client{
public:
    Client();

    void Connect();
    void Start();
    void Close();
private:
    struct sockaddr_in serverAddr;
    int server_fd;
    int fd[2];
    int epoll_fd;
    bool isWork;
    int pid;
    
    char message[MAXBUFF];
};

#endif