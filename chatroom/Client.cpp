//Client.cpp
#include "Client.h"

Client::Client()
{
    isWork = true;
    pid = 0;
    server_fd = 0;
    epoll_fd = 0;
    serverAddr.sin_family = PF_INET;
    serverAddr.sin_port = htons(SERVER_PORT);
    serverAddr.sin_addr.s_addr = INADDR_ANY;
}

void Client::Connect()
{
    cout<<"Connect Server:"<< inet_ntoa(serverAddr.sin_addr)<<":"<<ntohs(serverAddr.sin_port)<<endl;
    
    server_fd = socket(PF_INET, SOCK_STREAM, 0);
    
    if(server_fd < 0){
        perror("server_fd error");
        exit(-1);
    }
    
    if(connect(server_fd, (struct sockaddr *)&serverAddr, sizeof(serverAddr))<0 ){
        perror("connect error");
        exit(-1);
    }
    
    if(pipe(fd)<0){
        perror("make pipe error:");   
        exit(-1);
    }

    epoll_fd = epoll_create(EPOLL_SIZE);
    
    if(epoll_fd < 0) {
        perror("epoll_fd error");
        exit(-1); 
    }

    addfd(epoll_fd, server_fd, true);    
    addfd(epoll_fd, fd[0], true);
}


void Client::Start()
{
    static struct epoll_event ev[2];
    Connect();   
    pid = fork();
    
    if( pid == -1 ){
        perror("fork() error");
        exit(-1);
    }
    else if( pid == 0){ 
        close(fd[0]);
        while(isWork){           
            bzero(&message, MAXBUFF);
            fgets(message, MAXBUFF, stdin);
                
            if(strncasecmp(message, "EXIT", 4) == 0){
                isWork = false;                
            }
            else{
                if(write(fd[1],message,strlen(message))<0 ){        //问题：clientMessage是对“welcome to joint chartroom”的覆盖
                    perror("children process write message error");
                    exit(-1);
                }
            }
        }
    }
    else{   
        close(fd[1]);   
        while(isWork){
            int events_count = epoll_wait(epoll_fd, ev, 2, -1);
            if(events_count < 0){
                perror("epoll_wait error:");
                isWork = false;
            }
            
            for(int i =0; i<events_count;i++){
                bzero(&message, MAXBUFF);      
                if(ev[i].data.fd == server_fd){ 
                    int n = recv(server_fd, message, MAXBUFF, 0);
                    
                    if(n == 0){                
                        cout<<"close connet with "<<server_fd<<endl;    
                        close(server_fd);
                        isWork = false;
                    }
                    else{
                        message[n]='\0';                
                        cout<<message<<endl;
                        //<<"input message('exit' to exit):";
                    }
                }
                else{       
                    int ret = read(ev[i].data.fd, message, MAXBUFF);
                    
                    if(ret == 0)
                        isWork = false;
                    else 
                        send(server_fd, message, strlen(message), 0);
                }
            }
        }
    }
    Close();
}
void Client::Close()
{
    if(pid){
        close(fd[0]);
    }
    else    
        close(fd[1]);
        
    close(server_fd);
    close(epoll_fd);
}























