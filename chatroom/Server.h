//Server.h 
#ifndef SERVER_H
#define SERVER_H

#include "Common.h"
#include <list>

class Server{
public:
    Server();

    void Init();
    void Close();
    void Start();    
    int SentBoardcastMessage(int clientfd,char buff[]);
private:
    int         sock_fd;
    list<int>   clientfdList;
    struct      sockaddr_in serverAddr;
    int         epoll_fd;

};

#endif
