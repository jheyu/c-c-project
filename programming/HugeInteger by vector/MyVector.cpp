// Member-function definitions for class MyVector.
#include <iostream>
#include <iomanip>
#include "MyVector.h" // include definition of class MyVector 
using namespace std;

// --------------------------------- MyVecotr的重載運算子 --------------------------------- //
const MyVector &MyVector::operator=( const MyVector &right )   // 重載 assign 分配運算
{
	size = right.size;
	capacity = right.capacity;

	ptr = new int[ capacity ];

	for ( int i=0 ; i < size ; i++ )
		ptr[ i ] = right.ptr[ i ];

	return *this;
}

bool MyVector::operator==( const MyVector &right ) const       // 重載 等於equal 的運算
{
	if ( size == right.size ) // 若長度一樣
	{
		for ( int i=0 ; i < size ; i++ ) // 長度一樣則比每格裡面的內容相不相同
		{
			if ( ptr[ i ] != right.ptr[ i ] ) // 若不相同 , 則不用比了 , 跳出迴圈.....若比到最後還未跳出迴圈,則代表相等
				break;

			return true;
		}
		return false;
	}
	else  // 長度不一樣
	{
		return false;
	}
}

bool MyVector::operator!=( const MyVector &right ) const
{
	if ( operator==( right ) )  //----------------------------------------***************************
	{
		return false;
	}
	else
	{
		return true;
	}
}

int &MyVector::operator[]( unsigned int index )
{
	return ptr[ index ];
}

int MyVector::operator[]( unsigned int index ) const
{
	return ptr[ index ];
}
// --------------------------------- MyVecotr的重載運算子 --------------------------------- //





// default constructor
MyVector::MyVector()      // MyVecotr的建構子 / construct
{
   size = 0;
   capacity = 0;
   ptr = new int[ 0 ];             // 動態分配
} // end MyVector constructor

// constructor
MyVector::MyVector( unsigned int n, const int val )
{
   if( n > 0 )
   {
      ptr = new int[ n ];
      // initialize array to val
      for ( unsigned int i = 0; i < n; i++ )
         ptr[ i ] = val;

      size = n;
      capacity = n;
   }
   else
   {
      size = 0;
      capacity = 0;
   }
} // end MyVector constructor

// ----------- construct ------------ //
// 目的：將intArray中的內容取first到last這一段,並將這段的內容放到vector2中
MyVector::MyVector( int *first, int *last )
{
	size = last - first ;    // 就是 vector 中的元素有幾個

	ptr = new int[ size ];
	for ( unsigned int i = 0 ; i < size ; i++ )
	{
		ptr[ i ] = *( first + i );
	}
} // end construct

// ----------- construct ------------ //
// 目的：將傳入的vector的內容放入 這個物件的主人的內容 例如    MyVector vector5( vector2 ) , 是將 vector2複製到vector5中
MyVector::MyVector( const MyVector &x )
{
	ptr = new int[ x.capacity ];
	capacity = x.capacity;
	size     = x.size;

	for ( int i=0 ; i < size ; i++ )
		ptr[ i ] = x.ptr[ i ] ;
	
}

// destructor
MyVector::~MyVector()
{
   delete [] ptr;
} // end destructor

int* MyVector::begin()
{
   return ptr;
}

int* MyVector::end()
{
   return ptr + size;
}

int MyVector::getSize() const
{
   return size;
}

int MyVector::getCapacity() const
{
   return capacity;
}

bool MyVector::empty() const
{
   return ( size == 0 );
}

int& MyVector::front()
{
   return ptr[0];
}

int& MyVector::back()
{
   return ptr[ size - 1 ];
}

void MyVector::pop_back()
{
   size--;
}

void MyVector::clear()
{
   size = 0;
}
/*
void MyVector::display()
{
   for( unsigned int i = 0; i < size; i++ )
   {
      cout << setw(3) << integer[ i ];
      if( i % 10 == 9 || i == size - 1 )
         cout << endl;
   }
}
*/

// ----------- resize ----------- //
// 若 n = 5 , 實際容量 = 7 ... 把 0,1,2,3,4保留,將5,6刪除
// 若 n = 9 , 實際容量 = 7 ... 把 0,1,2,3,4,5,6保留,將7,8初始化為0
void MyVector::resize( unsigned int n )
{
	if ( n > size ) // 代表要擴大容量 
	{
		int *temp = new int[ n ];
		for ( int i=0 ; i < size ; i++ )  // 把原本陣列中的內容放入temp
		{
			temp[ i ] = ptr[ i ] ;
		}

		for ( int i=0 ; i < n-size ; i++ ) // 再補0
		{
			temp[ size + i ] = 0 ;
		}

		size = n;
		capacity = n;

		delete [] ptr;  // 將原本vector中的內容刪除
		ptr = new int[ capacity ]; // 動態分配一個capacity大小的陣列

		for ( int i=0 ; i < size ; i++ )
			ptr[ i ] = temp[ i ] ;   // 再將原本temp的內容一個一個放入vector的integer中

		delete [] temp;
	}
	else  // 代表size會縮小 , 但容量不縮小
	{
		int temp = size - n;
		size = size - temp;
	}
}

/*
// ------------------------------- 大數用 修正版resize -------------------------------- //
void MyVector::resize( unsigned int n )
{
	if ( n > size ) // 代表要擴大容量 
	{
		int *temp = new int[ n ];
		for ( int i=n - 1  ; i >= size-1  ; i-- )  // 把原本陣列中的內容放入temp
		{
			temp[ i ] = ptr[ i-size+1 ] ;
		}

		for ( int i=0 ; i < size -1 ; i++ ) // 再補0
		{
			temp[ i ] = 0 ;
		}

		size = n;
		capacity = n;

		delete [] ptr;  // 將原本vector中的內容刪除
		ptr = new int[ capacity ]; // 動態分配一個capacity大小的陣列

		for ( int i=0 ; i < size ; i++ )
			ptr[ i ] = temp[ i ] ;   // 再將原本temp的內容一個一個放入vector的integer中

		delete [] temp;
	}
	else  // 代表size會縮小 , 但容量不縮小
	{
		int temp = size - n;
		size = size - temp;
	}
}

*/
// ----------- resize ----------- //
void MyVector::resize( unsigned int n, const int val )
{
	if ( n > size ) // 代表要擴大容量 
	{
		int *temp = new int[ n ];
		for ( int i=0 ; i < size ; i++ )  // 把原本陣列中的內容放入temp
		{
			temp[ i ] = ptr[ i ] ;
		}

		for ( int i=0 ; i < n-size ; i++ ) // 再補 val
		{
			temp[ size + i ] = val ;
		}

		size = n;
		capacity = n;

		delete [] ptr;  // 將原本vector中的內容刪除
		ptr = new int[ capacity ]; // 動態分配一個capacity大小的陣列

		for ( int i=0 ; i < size ; i++ )
			ptr[ i ] = temp[ i ] ;   // 再將原本temp的內容一個一個放入vector的integer中

		delete [] temp;
	}
	else  // 代表size會縮小 , 但容量不縮小
	{
		int temp = size - n;
		size = size - temp;
	}
}


// ----------- assign ----------- //
// 分配新的內容給vector,並取代目前vector的內容並對應size修改..新的vector有n個元素,內容皆為val
void MyVector::assign( int n, const int val )
{
	if ( n > size )  // 代表擴大
	{
		delete [] ptr ;
		size = n;
		capacity = n;
		ptr = new int [ n ] ;
		for ( int i=0 ; i < size ; i++ )
			ptr[ i ] = val;
	}
	else // 代表縮小 
	{
		size = n ;
		for ( int i=0 ; i < n ; i++ )
			ptr[ i ] = val;
	}
}

// ----------- assign ----------- //
// 從intArray中取出first-last中的內容,取代目前主人的那段內容

void MyVector::assign( int *first, int *last )
{
	if ( last-first > size )  // 代表取出的那段比原本的vector大
	{
		int *temp = new int[ last-first ];
		for ( int i=0 ; i < last-first ; i++ )
			temp[ i ] = ptr[ i ];
		
		delete [] ptr;

		ptr = new int[ last-first ] ;
		for ( int i=0 ; i < last-first ; i++ )
			ptr[ i ] = temp[ i ];

		delete [] temp;

		capacity = last-first+1;

		
		delete [] ptr;
		size = last - first;
		ptr = new int [ last-first ];
		for ( int i=0 ; i < last-first ; i++ )
			ptr[ i ] = *( first + i ) ;
	
		
	}
	else // 代表取出的那段比原本的vector小
	{
		size = last - first;
		for ( int i=0 ; i < last-first ; i++ )
			ptr[ i ] = *( first + i ) ;
	}
}
/*


void MyVector::assign( int *first, int *last )
{
	if ( size-1 > last )  // 如果空間足夠 , 則將 對應的空間的內容替換
	{
		for ( int i=first ; i <= last ; i++ )
			integer[ i ] = *( first ++ );
	}
	else // 代表取出的那段比原本的vector大 < 則要重新分配一個較大的空間
	{
		int *temp = new int [ last+1 ];

		for ( int i=0 ; i < size ; i++ )
			temp[ i ] = integer[ i ];

		for ( int i=first ; i <= last ; i++ )
			temp[ i ] = *( first++ );
		
		
		delete [] integer;
		integer = new int[ last+1 ];
		capacity = last+1;
		size = last+1;
		for ( int i=0 ; i <= last ; i++ )
			integer[ i ] = temp[ i ];

		delete [] temp;
		
	}
}
*/

// ----------- reserve ----------- //
// 檢查容量是否足夠存放 n 個元素
// 如果n 大於現在的容量 , 分配給他一個 n 的容量給他,  ex : 原本9個元素 , n = 14 ..則要分配給可存放14個元素的空間
// else 不做任何事情!!

void MyVector::reserve( unsigned int n )
{
	if ( n > capacity )
	{
		int *temp = new int[ n ];
		for ( int i=0 ; i < size ; i++ )
			temp[ i ] = ptr[ i ];

		delete [] ptr;
		ptr = new int[ n ];

		for ( int i=0 ; i < size ; i++ )
			ptr[ i ] = temp[ i ];

		delete [] temp;
		capacity = n ;
	}
}

// 將容量縮小為和size相同 ... ex : size = 13   capacity = 16 , 將capacity 變為 13
void MyVector::shrink_to_fit()
{
	if ( capacity > size )
	{
		int *temp = new int[ size ];
		for ( int i=0 ; i < size ; i++ )
		{
			temp[ i ] = ptr[ i ];
		}
		delete ptr;
		ptr = new int[ size ];
		
		for ( int i=0 ; i < size ; i++ )
			ptr[ i ] = temp[ i ];

		delete [] temp;
		capacity = size;
	}
}

// 在原本vector的尾巴,在多加入一個val的元素 ,所以 size 和 capacity 皆會 ++
void MyVector::push_back( const int val )
{
	int *temp = new int[ size+1 ];
	for ( int i=0 ; i < size ; i++ )
		temp[ i ] = ptr[ i ];
	
	temp[ size ] = val;

	delete [] ptr;
	ptr = new int[ size+1 ];

	size = size +1;
	capacity = size;

	for ( int i=0; i < size ; i++ )
		ptr[ i ] = temp[ i ];

	delete [] temp;
}

int* MyVector::insert( int *position, const int val )
{
	int *temp = new int[ size+1 ];
	for ( int i=0 ; i < position - ptr ; i++ )
		temp[ i ] = ptr[ i ] ;
	
	temp[ position-ptr ] = val;

	for ( int i=position-ptr+1 ; i < size+1 ; i++ )
			temp[ i ] = ptr[ i-1 ];

	delete [] ptr ;

	ptr = new int[ size+1 ];

	size = size +1 ;
	capacity = size;

	for ( int i=0 ; i < size ; i++ )
	ptr[ i ] = temp[ i ];

	delete [] temp;
	
	return ptr;
}

// 代表從position 開始 加入 元素 (包含position ) 加入 n 個val值的元素
int* MyVector::insert( int *position, unsigned int n, const int val )
{
	int *temp = new int[ size+n ];
	for ( int i=0 ; i < position - ptr ; i++ )
		temp[ i ] = ptr[ i ];

	for ( int i=position-ptr ; i < position-ptr+n ; i++ )
		temp[ i ] = val;

	for ( int i=position-ptr+n ; i < size + n ; i++ )
		temp[ i ] = ptr[ i-n ];

	delete [] ptr;
	ptr = new int[ size+n ];

	size = size +n;
	capacity = size;
	
	for ( int i=0 ; i < size ; i++ )	
		ptr[ i ] = temp[ i ];

	delete [] temp;
	
	return ptr;
}

// 從 position那個陣列位址開始插入(包含position)
int* MyVector::insert( int *position, int *first, int *last )
{
	int *temp = new int[ size + (last - first) ];
	for ( int i=0 ; i < position - ptr ; i++ )
		temp[ i ] = ptr[ i ];

	for ( int i=position - ptr ; i < position - ptr + (last - first) ; i++ )
	{
		int counter = 0 ;
		temp[ i ] = *(first + counter );
		counter++;
	}

	for ( int i=position - ptr + (last - first) ; i < size + (last - first) ; i++ )
		temp[ i ] = ptr[ i - (last - first) ] ;


	delete [] ptr;

	ptr = new int[ size + (last-first) ];

	size = size + (last - first) ;
	capacity = size;

	for ( int i=0 ; i < size ; i++ )	
		ptr[ i ] = temp[ i ];

	delete [] temp;
	
	return ptr;
}

//
int* MyVector::erase( int *position )
{
	int *temp = new int[ size-1 ];

	for ( int i=0 ; i < position-ptr ; i++ )
		temp[ i ] = ptr[ i ];

	for ( int i=position-ptr ; i < size - 1 ; i++ )
	{
		temp[ i ] = ptr[ i+1 ];
	}

	delete [] ptr;
	ptr = new int[ size-1 ];

	size = size -1;
	capacity = size;

	for ( int i=0 ; i<size ; i++ )
		ptr[ i ] = temp[ i ];

	delete [] temp;

	return ptr;
}

int* MyVector::erase( int *first, int *last )
{
	int *temp = new int[ size - (last-first) ] ;

	for ( int i=0 ; i < first - ptr ; i++ )
		temp[ i ] = ptr[ i ] ;

	for ( int i=first-ptr ; i < size - (last-first) ; i++ )
		temp[ i ] = ptr[ i+(last-first) ];

	delete [] ptr;
	ptr = new int[ size - (last-first) ];
	size = size - ( last-first);
	capacity = size;

	for ( int i=0 ; i < size ; i++ )
		ptr[ i ] = temp[ i ];

	delete [] temp;
	return ptr;
}

void MyVector::swap( MyVector &x )
{
		int *temp4 = new int[ size ] ;  // 先把temp4的容量 放大為 vector5的容量
		int *temp5 = new int[ x.size ];     // 再把temp5的容量 縮小為 vector4的容量

		for ( int i=0 ; i < x.size ; i++ )  // 把 vector4的內容放到temp5
			temp5[ i ] = x.ptr[ i ];

		for ( int i=0 ; i < size ; i++ )   // 把 vector5的內容放到temp4
			temp4[ i ] = ptr[ i ] ;

		delete [] ptr;
		delete [] x.ptr;

		ptr = new int[ x.size ] ;
		x.ptr = new int[ size ];


		for ( int i=0 ; i < x.size ; i++ )
			ptr[ i ] = temp5[ i ];

		for ( int i=0 ; i < size ; i++ )
			x.ptr[ i ] = temp4[ i ];
		
		int buffer = size;
		size = x.size;
		x.size = buffer;

		delete [] temp4;
		delete [] temp5;
}