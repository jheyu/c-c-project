// Member-function definitions for class HugeInteger.
#include <iostream>
using namespace std;
#include "HugeInteger.h" // include definition of class HugeInteger 


ostream &operator<<( ostream &output, const HugeInteger &number )   // friend 函數很特殊:在定義中不須HugeInteger:: 甚至 friend 這個關鍵字都不用加入
{
	for ( int i= number.integer.getSize()-1  ; i >= 0 ; i-- )   // 為啥傑數條件改成 I==0 就不能了= =??
		output << number.integer[ i ];
		
	return output;
}


// default constructor
HugeInteger::HugeInteger( unsigned int size )
{
   if( size >= 0 )
      integer.insert( integer.begin(), size, 0 );
} // end HugeInteger constructor

// copy constructor
HugeInteger::HugeInteger( HugeInteger &integerToCopy )
   : integer( integerToCopy.integer )
{
} // end HugeInteger constructor

// randomly generate an integer
void HugeInteger::random()
{
   unsigned int size = integer.getSize();
   int *intArray = new int[ size ];

   intArray[ size - 1 ] = 1 + rand() % 9;
   for( unsigned int i = 0; i < size - 1; i++ )
      intArray[ i ] = rand() % 10;

   integer.assign( intArray, intArray + size );
} // end Random

// overloaded assignment operator;
// const return avoids: ( a1 = a2 ) = a3

const HugeInteger &HugeInteger::operator=( const HugeInteger &right )
{
   integer = right.integer;
   return *this; // enables x = y = z, for example
} // end function operator=


bool HugeInteger::operator==( const HugeInteger &op2 ) const
{
	if ( integer == op2.integer )
		return true;
	else
		return false;
}

bool HugeInteger::operator!=( const HugeInteger &op2 ) const
{
	if ( integer != op2.integer )
		return true;
	else
		return false;
}


bool HugeInteger::operator<( const HugeInteger &op2 ) const   // Less
{
	if( operator>( op2 ) )
		return false;

	if( operator==( op2 ) )
		return false;

	return true;
}

bool HugeInteger::operator>( const HugeInteger &op2 ) const   // Greater
{
	if( integer.getSize() > op2.integer.getSize() )   // this的size 比 op2的size大 
	{
		return true;
	}
	else if( integer.getSize() < op2.integer.getSize() ) // this的size 比 op2的size小
	{
		return false;
	}
	else
	{
		for( int i = op2.integer.getSize() - 1; i >= 0; i-- )  // this的size == op2的size
		{
			if( integer[ i ] > op2.integer[ i ] )
				return true;
			
			if( integer[ i ] < op2.integer[ i ] )
				return false;
		}
		return false;
	}
}

bool HugeInteger::operator<=( const HugeInteger &op2 ) const   // LessEqual
{
	if( operator>( op2 ) )
		return false;
	else
		return true;
}

bool HugeInteger::operator>=( const HugeInteger &op2 ) const   // GreaterEqual
{
	if( operator<( op2 ) )
		return false;
	else
		return true;
}


// addition operator; HugeInteger + HugeInteger
HugeInteger HugeInteger::operator+( const HugeInteger op2 )
{
   unsigned int size = integer.getSize();
   unsigned int op2Size = op2.integer.getSize();
   unsigned int sumSize = ( size >= op2Size ) ? size : op2Size;

   HugeInteger sum( sumSize + 1 ); // temporary result

   if( size <= op2Size )
   {
      // iterate through HugeInteger
      for( unsigned int i = 0; i < size; i++ )
         sum.integer[ i ] = integer[ i ] + op2.integer[ i ];

      for( unsigned int i = size; i < sumSize; i++ )
         sum.integer[ i ] = op2.integer[ i ];
   }
   else
   {
      // iterate through HugeInteger
      for( unsigned int i = 0; i < op2Size; i++ )
         sum.integer[ i ] = integer[ i ] + op2.integer[ i ];

      for( unsigned int i = op2Size; i < sumSize; i++ )
         sum.integer[ i ] = integer[ i ];
   }

   for( unsigned int i = 0; i < sumSize; i++ ) 
   {
      // determine whether to carry a 1
      if( sum.integer[ i ] > 9 ) 
      {
         sum.integer[ i ] -= 10; // reduce to 0-9
         sum.integer[ i + 1 ]++;
      } // end if
   } // end for

	if( sum.integer[ sumSize ] == 0 )
      sum.integer.resize( sumSize );

   return sum; // return the sum
}

   
HugeInteger HugeInteger::operator/( HugeInteger op2 )
{
	//int quoSize = size - op2.size + 1;    // 先取 proSize 為 被除數-除數 +1
	
	//int quoSize = op2.size;

    //------------------------------------------------------------------------
	HugeInteger remainder ( *this );   //remainder 由 被除數而來 ( 因位會動到)  /////----------**************


	int quoSize = integer.getSize() - op2.integer.getSize() + 1 ;

	HugeInteger quo( quoSize ); // temporary result 暫存的結果

	if( operator>=( op2 ) )   // 被除數 > 除數 -->進行運算
	{
		//HugeInteger buffer = op2;       // 把 op2 複製一份到buffer上面
		HugeInteger buffer ( op2 ) ;    // 把op2 複製一份到buffer上面

		//buffer.integer.size = size;  // 將bufferSize(除數的Size) 先定為和 integer.size(被除數的Size) 相同
		buffer.integer.resize( integer.getSize() );

		for ( int i=1 ; i <= op2.integer.getSize() ; i++ )   // 先將 除數的 數字平移 讓他和被除數同個位數
		{
			buffer.integer[ integer.getSize()-i ] = buffer.integer[ op2.integer.getSize() -i ] ;
		}


		for ( int i=0 ; i < integer.getSize()-op2.integer.getSize() ; i++ )   // 平移後補零
		{
			buffer.integer[ i ] = 0;
		}

		//-----***

		for ( int i=1 ; i <= quoSize ; i++ )
		{
			//while( remainder.greaterEqual( buffer ) )   // 開始一直將 remainder(也就是被除數) 一直不斷扣掉op2(也就是 除數) , 直到 (被除數 小於 除數) 且 (除數和原本未平移的除數相等)結束
			while ( remainder.operator>=( buffer ) )
			{			
				remainder = remainder.operator-( buffer );  // 將 remainder.integer - buffer.integer

				quo.integer[ quoSize-i ] ++;// 減完一次 將商在正確的位置 + 1
			}
			//remainder.integer.pop_back(); // --------********************* 做完那個位數 應該要退一位數 , size應該要減掉吧 (可是好像在subtract 裡面就有處理並判斷最高位是否為0了 所以不用做此動作)
			buffer.divideByTen();
		}
		if ( quo.integer[ quoSize-1 ] == 0 )
		{
			quoSize--;
			quo.integer.pop_back();
		}

	}
	else                        // 被除數 < 除數 -->不須進行除法直接return答案
	{
		quo.integer.clear();     // 將quo這個物件中的 MyVector integer 的 size 設為 0
		//quo.integer.size = 0;  // 將quo這個物件中的 MyVector integer 的 size 設為 0
	}
	return quo;
}


HugeInteger HugeInteger::operator-( HugeInteger &op2 )
{
   int subSize = ( integer.getSize() >= op2.integer.getSize() ) ? integer.getSize() : op2.integer.getSize();   // 取較大的當Sum的Size
   HugeInteger sub( subSize ); // temporary result 暫存的結果

   if( operator<( op2 ) )  // 如果op2 比較大 則用 op2 - integer
   {
      // iterate through HugeInteger	  
      for ( int i = 0; i < integer.getSize(); i++ )
         sub.integer[ i ] = op2.integer[ i ] - integer[ i ] ;

	  for ( int i = integer.getSize(); i < sub.integer.getSize() ; i++ )
         sub.integer[ i ] = op2.integer[ i ];
   }
   else         // 如果 integer 比較大
   {
      // iterate through HugeInteger
	   for ( int i = 0; i < op2.integer.getSize() ; i++ )
         sub.integer[ i ] = integer[ i ] - op2.integer[ i ];
	   
		for ( int i = op2.integer.getSize() ; i < sub.integer.getSize() ; i++ )
			sub.integer[ i ] = integer[ i ];
		
		
   }

   for ( int i = 0; i < sub.integer.getSize() -1 ; i++ )    // 判斷除了最高位之外的其他位數是否有 "負"的情況需要借位的
   {
      // determine whether to carry a 1
      if ( sub.integer[ i ] < 0 ) 
      {
         sub.integer[ i ] += 10; // reduce to 0-9
         sub.integer[ i + 1 ]--;
      } // end if
   } // end for

	while( sub.integer[ sub.integer.getSize()-1 ] == 0 )   // 單獨判斷最高位是否被借位借走 ==0 若是 , 則必須 size--
	{
		sub.integer.pop_back();
		// sub.integer.size--;  // *********************------------------------------**********************
	}


   return sub; // return the sum
}


HugeInteger HugeInteger::operator*( HugeInteger op2 )
{
	int proSize = integer.getSize() + op2.integer.getSize() ;    // 先取 proSize 為 乘數 和 被乘數的和
	HugeInteger pro( proSize ); // temporary result 暫存的結果

	for ( int i=0 ; i < integer.getSize() ; i++ )
	{
		for ( int j=0 ; j < op2.integer.getSize() ; j++ )
		{
			pro.integer[i+j] = pro.integer[i+j] + ( op2.integer[j] * integer[i] ) ;
		}
	}

	for ( int i=0 ; i < proSize ; i++ )
	{
		if ( pro.integer[i] > 9 )
		{
			int carry = pro.integer[i] / 10;  // 進到下一位數
			pro.integer[i]  = pro.integer[i] % 10;  // 留在本位的數字
			pro.integer[i+1] = pro.integer[i+1] + carry;
		}
	}

	if( pro.integer[ pro.integer.getSize() -1 ] == 0 ) 
	{
		pro.integer.pop_back();
	}
	return pro;
}

HugeInteger HugeInteger::operator%( HugeInteger op2 )
{
	//int quoSize = size - op2.size + 1;    // 先取 proSize 為 被除數-除數 +1
	
	//int quoSize = op2.size;

    //------------------------------------------------------------------------
	HugeInteger remainder ( *this );   //remainder 由 被除數而來 ( 因位會動到)  /////----------**************


	int quoSize = integer.getSize() - op2.integer.getSize() + 1 ;

	HugeInteger quo( quoSize ); // temporary result 暫存的結果

	if( operator>=( op2 ) )   // 被除數 > 除數 -->進行運算
	{
		//HugeInteger buffer = op2;       // 把 op2 複製一份到buffer上面
		HugeInteger buffer ( op2 ) ;    // 把op2 複製一份到buffer上面

		//buffer.integer.size = size;  // 將bufferSize(除數的Size) 先定為和 integer.size(被除數的Size) 相同
		buffer.integer.resize( integer.getSize() );

		for ( int i=1 ; i <= op2.integer.getSize() ; i++ )   // 先將 除數的 數字平移 讓他和被除數同個位數
		{
			buffer.integer[ integer.getSize()-i ] = buffer.integer[ op2.integer.getSize() -i ] ;
		}


		for ( int i=0 ; i < integer.getSize()-op2.integer.getSize() ; i++ )   // 平移後補零
		{
			buffer.integer[ i ] = 0;
		}

		//-----***

		for ( int i=1 ; i <= quoSize ; i++ )
		{
			//while( remainder.greaterEqual( buffer ) )   // 開始一直將 remainder(也就是被除數) 一直不斷扣掉op2(也就是 除數) , 直到 (被除數 小於 除數) 且 (除數和原本未平移的除數相等)結束
			while ( remainder.operator>=( buffer ) )
			{			
				remainder = remainder.operator-( buffer );  // 將 remainder.integer - buffer.integer

				quo.integer[ quoSize-i ] ++;// 減完一次 將商在正確的位置 + 1
			}
			//remainder.integer.pop_back(); // --------********************* 做完那個位數 應該要退一位數 , size應該要減掉吧 (可是好像在subtract 裡面就有處理並判斷最高位是否為0了 所以不用做此動作)
			buffer.divideByTen();
		}
		if ( quo.integer[ quoSize-1 ] == 0 )
		{
			quoSize--;
			quo.integer.pop_back();
		}

	}
	else                        // 被除數 < 除數 -->不須進行除法直接return答案
	{
		quo.integer.clear();     // 將quo這個物件中的 MyVector integer 的 size 設為 0
		//quo.integer.size = 0;  // 將quo這個物件中的 MyVector integer 的 size 設為 0
	}
	return remainder;
}




HugeInteger &HugeInteger::operator++()         // pre-fix 
{
	helpIncrement();
	return *this;
}

HugeInteger HugeInteger::operator++( int ) 
{
	HugeInteger temp ( *this );         // 呼叫 Copy Constructor 複製一份和 *this 一樣的物件 
													   // 禁止使用 HugeInteger temp = *this ( 會發生BUG 因為這裡有指標 ) !
	//HugeInteger temp = *this; // --------------------*********************************------------*********
	
	helpIncrement();
	
	return temp;
}

void HugeInteger::helpIncrement()              // 將 大數中的 末端 最低位數字 +1
{
	integer[ 0 ]++ ;             // 大數中的最低位數字+1

	for( unsigned int i = 0; i < integer.getSize() ; i++ )    // 判斷是否有進位問題
	{
      // determine whether to carry a 1
		if( integer[ i ] > 9 ) 
		{
			integer[ i ] -= 10; // reduce to 0-9
			integer[ i + 1 ]++;
		} // end if
	} // end for
}

// ----------------- private :: divideByTen() ----------------- // 
void HugeInteger::divideByTen()
{
	for( int i = 1; i < integer.getSize() ; i++ )
      integer[ i - 1 ] = integer[ i ];
  
	//integer[ integer.getSize() ] = 0; //----------------********
   integer.pop_back();
}
// ----------------- private :: divideByTen() ----------------- // 