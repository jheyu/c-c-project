// MyVector test program.
#include <iostream>
using namespace std;
#include <ctime>
#include "HugeInteger.h" // include definition of class HugeInteger 

int main()
{
   unsigned int seed;
   cout << "Enter seed: ";
   cin >> seed;
   cout << endl;

   srand( seed );
   // ----------------- 產生 n1 , n2 物件 , 並對其作初始設定 ---------------- // 
   unsigned int length1 = 10 + rand() % 21;
   unsigned int length2 = 10 + rand() % 21;
   HugeInteger n1( length1 ); // HugeInteger object n1
   HugeInteger n2( length2 ); // HugeInteger object n2
   n1.random();
   n2.random();
   // ----------------- End Of ------------------------------------------ //
   
   // checks for equality between n1 and n1 
   if( n1 == n1 )
      cout << n1 << " == " << n1 << endl << endl;

   // checks for equality between n1 and n2 
   if( n1 == n2 )
      cout << n1 << " == " << n2 << endl << endl;
	  
   
   // checks for inequality between n1 and n2
   if( n1 != n2 )
      cout << n1 << " != " << n2 << endl << endl;

   
   // tests for smaller number between n1 and n2
   if( n1 < n2 )
      cout << n1 << " < " << n2 << endl << endl;
   
   // tests for smaller number between n2 and n1
   if( n2 < n1 )
      cout << n2 << " < " << n1 << endl << endl;

   // tests for greater number between n1 and n2 
   if( n1 > n2 )
      cout << n1 << " > " << n2 << endl << endl;

   // tests for greater number between n2 and n1 
   if( n2 > n1 )
      cout << n2 << " > " << n1 << endl << endl;
    
   // tests for smaller or equal number between n1 and n1
   if( n1 <= n1 )
      cout << n1 << " <= " << n1 << endl << endl;

   // tests for smaller or equal number between n1 and n2
   if( n1 <= n2 )
      cout << n1 << " <= " << n2 << endl << endl;
    
   // tests for smaller or equal number between n1 and n1
   if( n2 <= n1 )
      cout << n2 << " <= " << n1 << endl << endl;

   // tests for greater or equal number between n2 and n2
   if( n2 >= n2 )
      cout << n2 << " >= " << n2 << endl << endl;
    
   // tests for greater or equal number between n1 and n2
   if( n1 >= n2 )
      cout << n1 << " >= " << n2 << endl << endl;

   // tests for greater or equal number between n2 and n2
   if( n2 >= n1 )
      cout << n2 << " >= " << n1 << endl << endl;
    
   cout << "----------------------------------------------------------------\n\n"; 
   
   // ----------------- 產生 n3 , n4 物件 , 並對其作初始設定 ---------------- // 
   unsigned int length3 = 10 + rand() % 21;
   unsigned int length4 = 5 + rand() % ( length3 - 5 );
	HugeInteger n3( length3 ); // HugeInteger object n3
   HugeInteger n4( length4 ); // HugeInteger object n4
   // ----------------- End Of ------------------------------------------ //

   n3.random();
   n4.random();
   
   // assigns the result of ( n3 / n4 ) to n5 then outputs n5
   HugeInteger n5; // HugeInteger object n5   
   n5 = n3 / n4;
   cout << n3 << " / " << n4 << " = " << n5 << endl << endl;
  
   
   // outputs the result of ( n4 * n5 ) to n6 then outputs n6
   HugeInteger n6; // HugeInteger object n6
   n6 = n4 * n5;  
   cout << n4 << " * " << n5 << " = " << n6 << endl << endl; 
   
   // assigns the result of ( n3 - n6 ) to n7 then outputs n7
   HugeInteger n7; // HugeInteger object n7
   n7 = n3 - n6;
   cout << n3 << " - " << n6 << " = " << n7 << endl << endl;
   
   // assigns the result of ( n3 % n4 ) to n8 then outputs n8
   HugeInteger n8; // HugeInteger object n8
   n8 = n3 % n4;
   cout << n3 << " % " << n4 << " = " << n8 << endl << endl;

   // checks for equality between n7 and n8 
   if( n7 == n8 )
      cout << n7 << " = " << n8 << endl << endl;

   // the result of ( n8 + n6 ) to n9 then outputs n9
   HugeInteger n9; // HugeInteger object n9
   n9 = n8 + n6;  
   cout << n8 << " + " << n6 << " = " << n9 << endl << endl;
   
   // checks for equality between n3 and n9 
   if( n3 == n9 )
      cout << n3 << " = " << n9 << endl << endl;
   
   cout << ++n9 << endl << endl;

   cout << n9++ << endl << endl;

   cout << n9 << endl << endl;

   ++n9 = n4;
   cout << n9 << endl << endl;
   
   system("PAUSE");
   return 0;
} // end main