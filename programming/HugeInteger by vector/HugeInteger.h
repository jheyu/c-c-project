// HugeInteger class definition.
#ifndef HUGEINTEGER_H
#define HUGEINTEGER_H
#include "MyVector.h" // include definition of class MyVector 

class HugeInteger 
{
//   friend istream &operator>>( istream &, HugeInteger & );
   friend ostream &operator<<( ostream &, const HugeInteger & );    // 重載輸出運算子
   //通常重載 << 運算子時，會使用friend函式，這是因為重載 << 函式時，它不是一個成員函式，理由在於 << 左邊不是物件，而是一個輸出串流，由於 << 函式不是成員函式，若要能存取資料成員，則該資料成員必須設定為public，使用friend函式來進行重載的話，則不用受此限制。 
   
public:
   HugeInteger( unsigned int = 0 ); // default constructor
   HugeInteger( HugeInteger &integerToCopy ); // copy constructor

   void random(); // randomly generate an integer
   const HugeInteger &operator=( const HugeInteger & ); // assignment operator

   bool operator==( const HugeInteger & ) const; // equal to
   bool operator!=( const HugeInteger & ) const; // not equal to
   bool operator<( const HugeInteger & ) const; // less than
   bool operator>( const HugeInteger & ) const; // greater than
   bool operator<=( const HugeInteger & ) const; // less than or equal
   bool operator>=( const HugeInteger & ) const; // greater than or equal to

   HugeInteger operator+( const HugeInteger op2 ); // addition operator; HugeInteger + HugeInteger
   HugeInteger operator-( HugeInteger &op2 ); // subtraction operator; HugeInteger - HugeInteger
   HugeInteger operator*( HugeInteger op2 ); // multiplication operator; HugeInteger * HugeInteger
   HugeInteger operator/( HugeInteger op2 ); // division operator; HugeInteger / HugeInteger
   HugeInteger operator%( HugeInteger op2 ); // modulus operator; HugeInteger % HugeInteger
   HugeInteger &operator++(); // prefix increment operator   前置 -- > 要加 & , 因為直接放進去改變本身的值
   HugeInteger operator++( int ); // postfix increment operator  後置
private:
   MyVector integer; // stores an integer
   void divideByTen();
   void helpIncrement();
}; // end class HugeInteger

#endif