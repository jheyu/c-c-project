// MyString class definition.
#ifndef MYSTRING_H
#define MYSTRING_H
using namespace std;

class MyString 
{
public:
	static const unsigned int npos = -1;

	MyString(); // Constructs an empty string, with a length of zero characters.
    MyString( const char *s ); // Copies the array of character (C-string) pointed by s.
    MyString( const char *s, unsigned int n ); // Copies the first n characters from the array of characters pointed by s.
	MyString( const MyString &str ); // Constructs a copy of str.
    MyString( const MyString &str, unsigned int pos, unsigned int len = npos );
	MyString( char *first, char *last ); // Copies the sequence of characters in the range [first,last), in the same order.
    MyString( unsigned int n, char c ); // Fills the string with n consecutive copies of character c.
    ~MyString(); // Destroys the string object.

	//const MyString &operator=( const MyString & ); // assignment operator
    bool operator==( const MyString & ) const; // test s1 == s2 
    //bool operator!=( const MyString &right ) const; // test s1 != s2
    //bool operator<( const MyString & ) const; // test s1 < s2
    bool operator>( const MyString &right ) const; // test s1 > s2
    //bool operator<=( const MyString &right ) const; // test s1 <= s2
    //bool operator>=( const MyString &right ) const; // test s1 >= s2
    char &operator[]( unsigned int ); // subscript operator (modifiable lvalue)
    char operator[]( unsigned int ) const; // subscript operator (rvalue)

	char* begin(); // Returns an pointer referring to the first character in the string.
	char* end(); // Returns an pointer referring to the past-the-end character of the string.

	unsigned int getSize() const; // Returns the number of characters in the string.
	void resize( unsigned int n ); // Resizes the string to a length of n characters.

	void resize( unsigned int n, char c ); // The new elements are initialized as copies of c.
	unsigned int getCapacity() const; // Returns the size of the storage space currently allocated for the string,
	void reserve( unsigned int n = 0 ); // Requests that the string capacity be enough to contain n characters.

	void clear(); // Erases the contents of the string, which becomes an empty string (with a length of 0 characters).
	bool empty() const; // Returns whether the string is empty (i.e. whether its size is 0).
	void shrink_to_fit(); // Requests the container to reduce its capacity to fit its size.
	char& back(); // Returns a reference to the last character of the string.
	char& front(); // Returns a reference to the first character of the string.

	MyString& append( const MyString &str, unsigned int subpos, unsigned int sublen );
	MyString& append( char *first, char *last );

	void push_back( char c ); // Appends character c to the end of the string, increasing its length by one.

	MyString& assign( unsigned int n, char c ); // Replaces the current value by n consecutive copies of character c.
	MyString& assign( char *first, char *last ); // Copies the sequence of characters in the range [first,last), in the same order.
	MyString& insert( unsigned int pos, const MyString &str, unsigned int subpos, unsigned int sublen );
	MyString& insert( unsigned int pos, const char *s, unsigned int n );

	char* insert( char *p, unsigned int n, char c );	
	char* insert( char *p, char c ); // The string is extended by inserting a character c before the character pointed by p,
	char* insert( char *p, char *first, char *last );	

	MyString& erase( unsigned int pos = 0, unsigned int len = npos );
	char* erase( char *p ); // Erases the character pointed by p.	
	char* erase( char *first, char *last ); // Erases the sequence of characters in the range [first,last).
	
	MyString& replace( const char *i1, const char *i2, const MyString& str );
	MyString& replace( unsigned int pos, unsigned int len, const MyString &str, unsigned int subpos, unsigned int sublen );
	MyString& replace( const char *i1, const char *i2, char *first, char *last );

	void swap( MyString &str );
	void pop_back(); // Erases the last character of the string, effectively reducing its size by one.
	const char* c_str(); // Returns a pointer to an array that contains a null-terminated sequence of characters
	unsigned int copy( char *s, unsigned int n, unsigned int pos = 0 );	
	unsigned int find( const MyString &str, unsigned int pos = 0 ) const;
	unsigned int find( const char *s, unsigned int pos, unsigned int n ) const;
	unsigned int rfind( const MyString &str, unsigned int pos = npos ) const;
	unsigned int rfind( const char *s, unsigned int pos, unsigned int n ) const;
	unsigned int find_first_of( const MyString str, unsigned int pos = 0 ) const;
	unsigned int find_first_of( const char *s, unsigned int pos, unsigned int n ) const;
	unsigned int find_last_of( const MyString &str, unsigned int pos = npos ) const;
	unsigned int find_last_of( const char *s, unsigned int pos, unsigned int n ) const;
	

	unsigned int find_first_not_of( const MyString &str, unsigned int pos = 0 ) const;
	unsigned int find_first_not_of( const char* s, unsigned int pos, unsigned int n ) const;
	unsigned int find_last_not_of( const MyString &str, unsigned int pos = npos ) const;
	unsigned int find_last_not_of( const char *s, unsigned int pos, unsigned int n ) const;

	MyString substr( unsigned int pos = 0, unsigned int len = npos ) const;
	
    int compare( unsigned int pos, unsigned int len, const MyString &str, unsigned int subpos, unsigned int sublen ) const;
	int compare( unsigned int pos, unsigned int len, const char *s, unsigned int n ) const;

	void display(); // Displays all elements in the string.
	
private:
	unsigned int size; // the number of elements in the string
	unsigned int capacity; // the size of the storage space currently allocated for the string, expressed in terms of elements.
	char *sPtr; // points to a dynamically allocated array which is used to store the elements of the string
}; // end class MyString

#endif