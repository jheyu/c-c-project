#include <iostream>
#include <iomanip>
#include "MyString.h"
using namespace std;

MyString::MyString()
{
	size = 0;
	capacity = 0;
	sPtr = new char[ 0 ];
}

MyString::MyString( const char *s )
{
	size = 0;
	capacity = 0;

	int i;
	for(i = 0; s[i] != '\0'; i++)
	{
		size++; 
		capacity++;
	}
	size = size + 1;
	capacity = capacity + 1;

	sPtr = new char[size];
	for(i = 0; s[i] != '\0'; i++)
		sPtr[i] = s[i];
	sPtr[i] = '\0';
}

MyString::MyString( const char *s, unsigned int n )
{
	size = n;
	capacity = n+1;
	sPtr = new char[capacity];

	int i;
	for(i = 0; s[i] != '\0'; i++)
		sPtr[i] = s[i];
	sPtr[i] = '\0';
}

MyString::MyString( const MyString &str )
{
	size = str.size;
	capacity = str.capacity;
	sPtr = new char[size];

	int i;
	for(i = 0; str.sPtr[i] != '\0'; i++)
		sPtr[i] = str.sPtr[i];
	sPtr[i] = '\0';
}


MyString::MyString( const MyString &str, unsigned int pos, unsigned int len)
{
	size = str.size;
	capacity = str.capacity;
	sPtr = new char[size];

	for(pos = 0; str.sPtr[pos] != '\0'; pos++)
		sPtr[pos] = str.sPtr[pos];
	sPtr[pos] = '\0';
	len = pos;
}

MyString::MyString( char *first, char *last )
{
	int i = 0;
	char *pos = first;
	for(;pos != last; pos++)
		i++;

	size = i + 1;
	capacity = i + 1;
	sPtr = new char[size];

	i = 0;
	for(pos = first; pos != last; pos++)
	{
		sPtr[i] = *pos;
		i++;
	}
	sPtr[i] = '\0';
}


MyString::MyString( unsigned int n, char c )
{
	size = n + 1;
	capacity = n + 1;
	sPtr = new char[size];

	int i;
	for(i = 0; i < n; i++)
		sPtr[i] = c;
	sPtr[i] = '\0';
}

MyString::~MyString()
{
	delete [] sPtr;
}

char MyString::operator[](unsigned int n) const
{
	return this->sPtr[n];
}

char& MyString::operator[]( unsigned int n)
{
	return this->sPtr[n];
}

bool MyString::operator==( const MyString &str ) const
{
	if(this->size == str.size)
	{
		for(int i = 0; i < str.size; i++)
		{
			if(this->sPtr[i] != str.sPtr[i])
			{
				return false;
			}
		}
		return true;
	}
	else
	{
		return false;
	}
}

bool MyString::operator>( const MyString &right ) const
{
	if(this->size > right.size)
	{
		return true;
	}
	else if(this->size == right.size)
	{
		for(int i = 0; i < this->size; i++)
		{
			if(this->sPtr[i] < right.sPtr[i])
			{
				return false;
			}
		}
	}
	else
	{
		return false;
	}

	return true;
}

char* MyString::begin()
{
	return &sPtr[0];
}

char* MyString::end()
{
	return &sPtr[size-1];
}

unsigned int MyString::getSize() const
{
	return size;
}

void MyString::resize( unsigned int n )
{
	int i;

	if(n >= size)
	{
		char *str = new char[n + 1];

		for(i = 0; i < size; i++)
			str[i] = sPtr[i];
		str[i] = '\0';

		sPtr = new char[n + 1];
		capacity = n;
		sPtr = str;
	}
	else
	{
		char *str = new char[n + 1];

		for(i = 0; i < n; i++)
			str[i] = sPtr[i];
		str[i] = '\0';

		sPtr = new char[n + 1];
		size = n - 1;
		capacity = n;
		sPtr = str;
	}

}


void MyString::resize( unsigned int n, char c )
{
	int i;

	if(n >= size)
	{
		sPtr = new char[n + 1];
		size = n;
		capacity = n;

		for(i = 0; i < n; i++)
			sPtr[i] = c;
		sPtr[i] = '\0';
	}
	else
	{
		sPtr = new char[n + 1];
		size = n - 1;
		capacity = n;

		for(i = 0; i < n; i++)
			sPtr[i] = c;
		sPtr[i] = '\0';
	}
}

unsigned int MyString::getCapacity() const
{
	return capacity;
}

void MyString::reserve( unsigned int n)
{
	if(size < n)
	{
		MyString resize;

		resize.sPtr = new char [n];
		for(unsigned int i = 0; i < size; i++)
			resize.sPtr[i] = sPtr[i];

		resize.size = size;
		resize.capacity = capacity;

		this->sPtr = new char [n];
		for(unsigned int i = 0; i < size; i++)
			this->sPtr[i] = resize.sPtr[i];

		this->size = size;
		this->capacity = n;
	}
}

void MyString::clear()
{
	this->sPtr = new char [capacity];
	this->size = 0;
}

bool MyString::empty() const
{
	if(this->size == 0)
		return true;
	else
		return false;
}


void MyString::shrink_to_fit()
{
	if(capacity > size)
	{
		MyString resize;

		resize.sPtr = new char [size];
		resize.size = size;
		for(unsigned int i = 0; i < size; i++)
			resize.sPtr[i] = sPtr[i];

		this->sPtr = new char [resize.size];
		for(unsigned int i = 0; i < size; i++)
			this->sPtr[i] = resize.sPtr[i];

		this->size = resize.size;
		this->capacity = resize.size;
	}
}


char& MyString::back()
{
	return sPtr[size - 1];
}

char& MyString::front()
{
	return sPtr[0];
}

void MyString::push_back( char c )
{
	if(capacity > size)
	{
		sPtr[size] = c;
		size++;
	}
	else
	{
		MyString resize;

		resize.sPtr = new char [size + 1];
		for(unsigned int i = 0; i < size; i++)
			resize.sPtr[i] = sPtr[i];

		resize.sPtr[size] = c;
		resize.size = size + 1;
		resize.capacity = size + 1;

		this->sPtr = new char [resize.size];
		for(unsigned int i = 0; i < resize.size; i++)
			this->sPtr[i] = resize.sPtr[i];

		this->size = resize.size;
		this->capacity = resize.capacity;
	}
}

MyString& MyString::assign( unsigned int n, char c )
{
	unsigned int i;
	for(i = 0; i < n; i++)
		sPtr[i] = c;
	return *this;
}

MyString& MyString::assign( char *first, char *last )
{
	unsigned int i;
	if(size > 0)
	{
		for(unsigned int i = 0; i < size; i++)
			sPtr[i] = *(first + i);
	}
	else
	{
		size = 0;
		capacity = 0;
	}

	return *this;
}

MyString& MyString::append( const MyString &str, unsigned int subpos, unsigned int sublen )
{
	unsigned int length = sublen - subpos;
	MyString resize;

	resize.sPtr = new char [size + length + 1];
	resize.size = size + length;
	resize.capacity = size + length + 1;

	unsigned int i;
	for(i = 0; i < size; i++)
		resize.sPtr[i] = sPtr[i];
	for( ; i < size + length; i++)
		resize.sPtr[i] = str.sPtr[length - (resize.size - i)];
	resize.sPtr[i] = '\0';

	//for(i = 0; i < size + length; i++)
	//cout << resize.sPtr[i];
	//cout << endl;

	sPtr = new char [resize.size + length + 1];
	size = resize.size;
	capacity = resize.capacity;

	for(i = 0; i < size; i++)
		sPtr[i] = resize.sPtr[i];
	sPtr[i] = '\0';

	return resize;
}


MyString& MyString::append( char *first, char *last )
{
	unsigned int length = last - first + 1;
	MyString resize;

	resize.sPtr = new char [size + length + 1];
	resize.size = size + length;
	resize.capacity = size + length + 1;

	unsigned int i;
	for(i = 0; i < size; i++)
		resize.sPtr[i] = sPtr[i];
	for( ; i < resize.size; i++)
		resize.sPtr[i] = *(last - (resize.size - i) + 1);
	resize.sPtr[i] = '\0';

	//for(i = 0; i < size + length; i++)
	//cout<<resize.sPtr[i];
	//cout<<endl;

	sPtr = new char [resize.capacity];
	size = resize.size;
	capacity = resize.capacity;

	for(i = 0; i < size; i++)
		sPtr[i] = resize.sPtr[i];
	sPtr[i] = '\0';

	return resize;
}


MyString& MyString::insert( unsigned int pos, const MyString &str, unsigned int subpos, unsigned int sublen )
{
	int sizetemp = sublen - subpos;
	MyString temp;
	temp.sPtr = new char [sizetemp + 1];
	temp.size = sizetemp;
	temp.capacity = sizetemp + 1;

	unsigned int i;
	for(i = 0; i < sizetemp; i++)
		temp.sPtr[i] = str.sPtr[i];
	temp.sPtr[i] = '\0';

	int tempSize = this->size + sizetemp;
	MyString tempStr;
	tempStr.sPtr = new char [tempSize];
	tempStr.size = tempSize - 1;
	tempStr.capacity = tempSize;

	for(i = 0; i < tempStr.size; i++)
	{
		if(pos == 0)
		{
			if(i < temp.size)
			{
				tempStr.sPtr[i] = temp.sPtr[i];
			}
			else
			{
				tempStr.sPtr[i] = sPtr[i - temp.size];
			}
		}
		else if(pos > 0 && pos < this->size-1)
		{
			if(i < sizetemp) 
			{
				//cout << "if " << i << endl;
				tempStr.sPtr[i] = sPtr[i];
			}
			else if(i >= pos && i < sizetemp*2)
			{
				//cout << "else if " << i << endl;
				tempStr.sPtr[i] = temp.sPtr[i - sizetemp];
			}
			else
			{
				//cout << "else " << i << endl;
				tempStr.sPtr[i] = sPtr[i - sizetemp + 1];
			}
		}
		else
		{
			if(i < this->size - 1)
			{
				tempStr.sPtr[i] = sPtr[i];
			}
			else
			{
				tempStr.sPtr[i] = temp.sPtr[i - (this->size - 1)];
			}
		}
	}
	tempStr.sPtr[i] = '\0';

	this->sPtr = new char [tempStr.capacity];//tempStr.sPtr = new char [tempSize];
	this->size = tempSize;
	this->capacity = tempStr.capacity;
	for(i = 0; i < tempStr.size; i++)
	{
		this->sPtr[i] = tempStr.sPtr[i];
	}
	this->sPtr[i] = '\0';

	return *this;
}

MyString& MyString::insert( unsigned int pos, const char *s, unsigned int n )
{
	int sizetemp = n;
	MyString temp;
	temp.sPtr = new char [sizetemp + 1];
	temp.size = sizetemp;
	temp.capacity = sizetemp + 1;

	unsigned int i;
	for(i = 0; i < sizetemp; i++)
		temp.sPtr[i] = s[i];
	temp.sPtr[i] = '\0';

	int tempSize = this->size + sizetemp;
	MyString tempStr;
	tempStr.sPtr = new char [tempSize];
	tempStr.size = tempSize - 1;
	tempStr.capacity = tempSize;

	for(i = 0; i < tempStr.size; i++)
	{
		if(pos == 0)
		{
			if(i < temp.size)
			{
				tempStr.sPtr[i] = temp.sPtr[i];
			}
			else
			{
				tempStr.sPtr[i] = sPtr[i - temp.size];
			}
		}
		else if(pos > 0 && pos < this->size-1)
		{
			if(i < sizetemp) 
			{
				//cout << "if " << i << endl;
				tempStr.sPtr[i] = sPtr[i];
			}
			else if(i >= pos && i < sizetemp*2)
			{
				//cout << "else if " << i << endl;
				tempStr.sPtr[i] = temp.sPtr[i - sizetemp];
			}
			else
			{
				//cout << "else " << i << endl;
				tempStr.sPtr[i] = sPtr[i - sizetemp + 1];
			}
		}
		else
		{
			if(i < this->size - 1)
			{
				tempStr.sPtr[i] = sPtr[i];
			}
			else
			{
				tempStr.sPtr[i] = temp.sPtr[i - (this->size - 1)];
			}
		}
	}
	tempStr.sPtr[i] = '\0';

	this->sPtr = new char [tempStr.capacity];
	this->size = tempSize;
	this->capacity = tempStr.capacity;
	for(i = 0; i < tempStr.size; i++)
	{
		this->sPtr[i] = tempStr.sPtr[i];
	}
	this->sPtr[i] = '\0';

	return *this;
}


char* MyString::insert( char *p, unsigned int n, char c )
{
	MyString temp;
	temp.sPtr = new char [this->size + 3];
	temp.size = this->size + 3;
	temp.capacity = this->capacity + 3;

	unsigned int pos;
	for(pos = 0; pos < this->size; pos++)
	{
        if(this->sPtr[pos] == *p)
		     break;
	}
	
	unsigned int i;
	for(i = 0; i < temp.size - 1; i++)
	{
		if(pos == 0)
		{
			if(i >= pos && i < pos+n)
			{
				for(int k = i; k < i + n; k++)
				     temp.sPtr[k] = c;
				i = i + n-1;
			}
			else
			{
				temp.sPtr[i] = sPtr[i - n];
			}
		}
		else if(pos > 0 && pos < this->size-1)
		{
			if(i < pos) 
			{
				//cout << "if " << i << endl;
				temp.sPtr[i] = sPtr[i];
			}
			else if(i >= pos && i < pos+n)
			{
				//cout << "else if " << i << endl;
				for(int k = i; k < i + n; k++)
				   temp.sPtr[k] = c;
				i = i + n-1;
			}
			else
			{
				//cout << "else " << i << endl;
				temp.sPtr[i] = sPtr[i - n];
			}
		}
		else
		{
			if(i < this->size - 1)
			{
				temp.sPtr[i] = sPtr[i];
			}
			else
			{
				for(int k = i; k < i + n; k++)
				   temp.sPtr[k] = c;
				i = i + n-1;
			}
		}
	}
	temp.sPtr[i] = '\0';
	
	this->sPtr = new char [temp.size];
	this->size = temp.size;
	this->capacity = temp.size;

	for(i = 0; i < temp.size - 1; i++)
		this->sPtr[i] = temp.sPtr[i];
	this->sPtr[i] = '\0';

	return this->sPtr;
}


char* MyString::insert( char *p, char c )
{
	unsigned int n = 1;
	MyString temp;
	temp.sPtr = new char [this->size + 3];
	temp.size = this->size + 3;
	temp.capacity = this->capacity + 3;

	unsigned int pos;
	for(pos = 0; pos < this->size; pos++)
	{
        if(this->sPtr[pos] == *p)
		     break;
	}
	
	unsigned int i;
	for(i = 0; i < temp.size - 1; i++)
	{
		if(pos == 0)
		{
			if(i == pos)
			{
				for(int k = i; k < i + n; k++)
				     temp.sPtr[k] = c;
				i = i + n-1;
			}
			else
			{
				temp.sPtr[i] = sPtr[i - n];
			}
		}
		else if(pos > 0 && pos < this->size-1)
		{
			if(i < pos) 
			{
				//cout << "if " << i << endl;
				temp.sPtr[i] = sPtr[i];
			}
			else if(i >= pos && i < pos+n)
			{
				//cout << "else if " << i << endl;
				for(int k = i; k < i + n; k++)
				   temp.sPtr[k] = c;
				i = i + n-1;
			}
			else
			{
				//cout << "else " << i << endl;
				temp.sPtr[i] = sPtr[i - n];
			}
		}
		else
		{
			if(i < this->size - 1)
			{
				temp.sPtr[i] = sPtr[i];
			}
			else
			{
				for(int k = i; k < i + n; k++)
				   temp.sPtr[k] = c;
				i = i + n-1;
			}
		}
	}
	temp.sPtr[i] = '\0';
	
	this->sPtr = new char [temp.size];
	this->size = temp.size;
	this->capacity = temp.size;

	for(i = 0; i < temp.size - 1; i++)
		this->sPtr[i] = temp.sPtr[i];
	this->sPtr[i] = '\0';

	return this->sPtr;
}

char* MyString::insert( char *p, char *first, char *last )
{
	char *str = new char [last - first + 1];
	unsigned int i;
	for(i = 0; first <= last; i++,first++)
		str[i] = *first;
	str[i] = '\0'; 

	MyString temp;
	int length = i - 1;
	int n = this->size + i - 1;

	temp.sPtr = new char [n];
	temp.size = n;
	temp.capacity = n;

	unsigned int pos;
	for(pos = 0; pos < this->size; pos++)
	{
        if(this->sPtr[pos] == *p)
		     break;
	}
	//cout << temp.size << endl;
	//system("pause");
	for(i = 0; i < temp.size - 1; i++)
	{
		if(pos == 0)
		{
			if(i >= pos && i < pos+length)
			{
				for(int k = 0; k < length; k++)
				     temp.sPtr[k] = str[k];
				i = i + length - 1;
			}
			else
			{
				temp.sPtr[i] = sPtr[i - length];
			}
		}
		else if(pos > 0 && pos < this->size-1)
		{
			//cout << "test" << endl;
			if(i < pos) 
			{
				//cout << "if " << i << endl;
				temp.sPtr[i] = sPtr[i];
			}
			else if(i >= pos && i < pos+length)
			{
				//cout << "else if " << i << endl;
				for(int k = 0; k < length; k++,i++)
				   temp.sPtr[i] = str[k];
				i = i - 1;
			}
			else
			{
				//cout << "else " << i << endl;
				temp.sPtr[i] = sPtr[i - length];
			}
		}
		else
		{
			if(i < this->size - 1)
			{
				//cout << i << endl;
				temp.sPtr[i] = sPtr[i];
			}
			else
			{
				//cout << i << endl;
				for(int k = 0; k < length; k++,i++)
				   temp.sPtr[i] = str[k];
				i = i - 1;
			}
		}
	}
	temp.sPtr[i] = '\0';
	
	this->sPtr = new char [temp.size];
	this->size = temp.size;
	this->capacity = temp.size;

	for(i = 0; i < temp.size - 1; i++)
		this->sPtr[i] = temp.sPtr[i];
	this->sPtr[i] = '\0';

	return this->sPtr;
}


MyString& MyString::erase( unsigned int pos, unsigned int len )
{
	MyString temp;
	temp.sPtr = new char [this->size - len];
	temp.size = this->size - len;
	temp.capacity = this->size - len;
	
	unsigned int i;
	for(i = 0; i < this->size - 1; i++)
	{
		if(i < pos)
		{
			temp.sPtr[i] = this->sPtr[i];
		}
		else if(i > pos + len - 1)
		{
			temp.sPtr[i - len] = this->sPtr[i];
		}
	}
	temp.sPtr[i - len] = '\0';
    
	this->sPtr = new char [temp.size];
	this->size = temp.size;
	this->capacity = temp.size;

	for(i = 0; i < this->size - 1; i++)
		this->sPtr[i] = temp.sPtr[i];
	this->sPtr[i] = '\0';

	return *this;
}


char* MyString::erase( char *p )
{
	unsigned int pos;
	for(pos = 0; pos < this->size; pos++)
	{
        if(this->sPtr[pos] == *p)
		     break;
	}

	MyString temp;
	temp.sPtr = new char [this->size - pos];
	temp.size = this->size - pos;
	temp.capacity = this->size - pos;
	
	unsigned int i;
	for(i = 0; i < this->size - 1; i++)
	{
		if(i < pos)
		{
			temp.sPtr[i] = this->sPtr[i];
		}
		else if(i > pos + pos - 1)
		{
			temp.sPtr[i - pos] = this->sPtr[i];
		}
	}
	temp.sPtr[i - pos] = '\0';
    
	this->sPtr = new char [temp.size];
	this->size = temp.size;
	this->capacity = temp.size;

	for(i = 0; i < this->size - 1; i++)
		this->sPtr[i] = temp.sPtr[i];
	this->sPtr[i] = '\0';
	
	return this->sPtr;
}


char* MyString::erase( char *first, char *last )
{
	unsigned int pos, posf, post;
	for(pos = 0; pos < this->size; pos++)
	{
        if(&this->sPtr[pos] == first)
		     posf = pos;
		if(&this->sPtr[pos] == last)
			post = pos;
	}
    
	MyString temp;
	temp.sPtr = new char [this->size - (post - posf)];
	temp.size = this->size - (post - posf);
	temp.capacity = this->size - (post - posf);
	
	unsigned int i;
	for(i = 0; i < this->size - 1; i++)
	{
		if(i < posf)
		{
			temp.sPtr[i] = this->sPtr[i];
		}
		else if(i > post-1)
		{
			temp.sPtr[i - (post - posf)] = this->sPtr[i];
		}
	}
	temp.sPtr[i - (post - posf)] = '\0';
    
	this->sPtr = new char [temp.size];
	this->size = temp.size;
	this->capacity = temp.size;

	for(i = 0; i < this->size - 1; i++)
		this->sPtr[i] = temp.sPtr[i];
	this->sPtr[i] = '\0';

	return this->sPtr;
}

/*
MyString& MyString::replace( const char *i1, const char *i2, const MyString& str )
{
}

MyString& MyString::replace( unsigned int pos, unsigned int len, const MyString &str, unsigned int subpos, unsigned int sublen )
{
}

MyString& MyString::replace( const char *i1, const char *i2, char *first, char *last )
{
}

void MyString::swap( MyString &str )
{
}
*/
void MyString::pop_back()
{
	size--;
}

const char* MyString::c_str()
{
	return this->sPtr;
}


unsigned int MyString::copy( char *s, unsigned int n, unsigned int pos )
{
	unsigned int i;
	for(i = 0; i < this->size; i++)
	{
		if(i >= pos && i < pos + 6)
		{
			s[i - n + 1] = this->sPtr[i];
		}
	}
	return n;
}


unsigned int MyString::find( const MyString &str, unsigned int pos ) const
{
	unsigned int i, beg = 0;
	bool Nofound = true;

	for(i = pos; i < this->size; i++)
	{
		//cout << i << endl;
		if(sPtr[i] == str.sPtr[0])
		{
			beg = i;
			int k, temp = i;
			for(k = 0; str.sPtr[k] != '\0'; k++)
			{
				//cout <<str.size << ' '<< sPtr[temp] << ' ' << str.sPtr[k] << ' ' << k << endl;
				if(sPtr[temp] == str.sPtr[k])
					temp++;
				else
				{
					Nofound = true;
					break;
				}
			}
			if(k == str.size - 1)
			{
				Nofound = false;
				break;
			}
		}
	}

	if(Nofound == false)
	    return beg;
	else
		return -1;
}

/*
unsigned int MyString::find( const char *s, unsigned int pos, unsigned int n ) const
{
}


unsigned int MyString::rfind( const MyString &str, unsigned int pos = npos ) const
{
}

unsigned int MyString::rfind( const char *s, unsigned int pos, unsigned int n ) const
{
}

unsigned int MyString::find_first_of( const MyString str, unsigned int pos = 0 ) const
{
}

unsigned int MyString::find_first_of( const char *s, unsigned int pos, unsigned int n ) const
{
}

unsigned int MyString::find_last_of( const MyString &str, unsigned int pos = npos ) const
{
}

unsigned int MyString::find_last_of( const char *s, unsigned int pos, unsigned int n ) const
{
}

MyString MyString::substr( unsigned int pos = 0, unsigned int len = npos ) const
{
}

int MyString::compare( unsigned int pos, unsigned int len, const MyString &str, unsigned int subpos, unsigned int sublen ) const
{
}

int MyString::compare( unsigned int pos, unsigned int len, const char *s, unsigned int n ) const
{
}
*/

void MyString::display()
{
	for(unsigned int i = 0; i < size; i++)
		cout << sPtr[i];
	cout << endl;
}