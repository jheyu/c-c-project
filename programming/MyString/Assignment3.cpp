#include <iostream>
#include <iomanip>
#include <ctime>
#include "MyString.h"
using namespace std;

int main()
{
	char str[26] = "Initial the string shit!!";

	MyString string1(str);
	cout << "string1:";
	string1.display();
	cout << string1[0] << endl;
	string1[0] = 'A';
	string1.display();
	MyString sttr(string1);
	sttr[8] = 'v';
	if(string1 == sttr)
		cout << "Equal" << endl;
	else
		cout << "Not Equal" << endl;

	if(string1 > sttr)
        cout << "bigger" << endl;
	else
		cout << "smaller" << endl;
	system("pause");

	MyString string2("qwertasdtyuo",12);
	cout << "string2:";
	string2.display();
	
	MyString string3(string2);
	cout << "string3:";
	string3.display();
	
	cout << endl;

	cout << "string3.end(): " << string3.end() << endl;
	cout << "string3.getSize(): " << string3.getSize() << endl;
	cout << "string3.getCapacity(): " << string3.getCapacity() << endl;
	
	cout << endl;

	string3.resize(20);
	cout << "string3.resize(20): ";
	string3.display();
	cout << "string3.getSize(): " << string3.getSize() << endl;
	cout << "string3.getCapacity(): " << string3.getCapacity() << endl;

	cout << endl;

	cout << "string3.resever(): ";
	string3.reserve(15);
	string3.display();
	cout << "string3.getSize(): " << string3.getSize() << endl;
	cout << "string3.getCapacity(): " << string3.getCapacity() << endl;

	cout << endl;

	cout << "string3.resize(8): ";
	string3.resize(8);
	string3.display();
	cout << "string3.getSize(): " << string3.getSize() << endl;
	cout << "string3.getCapacity(): " << string3.getCapacity() << endl;

	cout << endl;
	MyString string4(str);
	cout << "string4:";
	string4.display();
	cout << "string4.getSize(): " << string4.getSize() << endl;
	cout << "string4.getCapacity(): " << string4.getCapacity() << endl;

	cout << endl;

    cout << "string4.resize(15): ";
	string4.resize(15);
	string4.display();
	cout << "string4.getSize(): " << string4.getSize() << endl;
	cout << "string4.getCapacity(): " << string4.getCapacity() << endl;

	cout << endl;

    cout << "string4.resize(20): ";
	string4.resize(20);
	string4.display();
	cout << "string4.getSize(): " << string4.getSize() << endl;
	cout << "string4.getCapacity(): " << string4.getCapacity() << endl;

	cout << endl;

    cout << "string4.shrink_to_fit(): ";
	string4.shrink_to_fit();
	string4.display();
	cout << "string4.getSize(): " << string4.getSize() << endl;
	cout << "string4.getCapacity(): " << string4.getCapacity() << endl;
	
	cout << endl;

	cout << "string4.push_back(): ";
	string4.push_back('Y');
	string4.display();

	cout << endl;

	cout << "string4.assign(5,'W'): ";
	string4.assign(5,'W');
	string4.display();

	cout << endl;
	
	char *first = str + 0;
	char *last = str + 5;
	cout << "string4.assign(first,last): ";
	string4.assign(first,last);
	string4.display();
	
	cout << endl;

	cout << "string4.append(string3,0,5): ";
	string4.append(string3,0,5);
	string4.display();

	cout << endl;

	first = str + 7;
	last = str + 11;
	cout << "string4.append(first,last): ";
	string4.append(first,last);
	string4.display();

	cout << endl;
	
	MyString string5(str);
	cout << "string5.insert(0, str, 0, 7): ";
	string5 = string5.insert(0, string4, 0, 7);
	string5.display();

	cout << endl;
	
	MyString string6(str);
	cout << "string6.insert(25, str, 7): ";
	string6 = string6.insert(25, str, 7);
	string6.display();

	cout << endl;

	MyString string7("to be quenstion");
	cout << "string7.insert(string7.end(), 3, '.'): ";
	string7.insert(string7.end(), 3, '.');
	string7.display();

	cout << endl;
	
	MyString string8("to be quenstion");
	cout << "string8.insert(string8.begin(), '.'): ";
	string8.insert(string8.begin(), '.');
	string8.display();

	cout << endl;
	
	MyString string9("to be quenstion");
	MyString string10("or not to be");
	
	cout << "string9.insert(string9.begin() + 5, string10.begin(), string10.begin() + 3): ";
	string9.insert(string9.begin() + 5, string10.begin(), string10.begin() + 3);
	string9.display();

	cout << endl;

	MyString string11("Test string...");
	cout << "string11.c_str(): ";
	string11.c_str();
	string11.display();

	cout << endl;

	char buffer[20];
	size_t length = string11.copy(buffer,6,5);
	buffer[length] = '\0';
	cout << "buffer contains: " << buffer << endl;

	cout << endl;

	MyString string12("This is an example sentence.");
	cout << "string12.erase(10,8): ";
	string12.erase(10,8);
	string12.display();

	cout << endl;

	MyString string13("This is an example sentence.");
	cout << "string13.erase(string13.begin()+9): ";
	string13.erase(string13.begin()+9);
	string13.display();

	cout << endl;

	MyString string14("This is an example sentence.");
	cout << "string14.erase(string14.begin()+5, string14.end()-9): ";
	string14.erase(string14.begin()+5, string14.end()-9);
	string14.display();

	cout << endl;

	MyString string15("This is an example sentence.");
	MyString string16("an");
	cout << "string15.find(string16,3): ";
	cout << string15.find(string16,3) << endl;

	system("pause");
	return 0;
}