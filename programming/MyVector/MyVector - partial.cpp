// Member-function definitions for class MyVector.
#include <iostream>
#include <iomanip>
#include "MyVector.h" // include definition of class MyVector 
using namespace std;

// default constructor
MyVector::MyVector()
{
	size = 0;
	capacity = 0;
	integer = new int[ 0 ];
} // end MyVector constructor

// constructor
MyVector::MyVector( unsigned int n, const int val )
{
	if( n > 0 )
	{
		integer = new int[ n ];
		// initialize array to val
		for ( unsigned int i = 0; i < n; i++ )
			integer[ i ] = val;

		size = n;
		capacity = n;
	}
	else
	{
		size = 0;
		capacity = 0;
	}
} // end MyVector constructor

MyVector::MyVector( int *first, int *last )
{
	size = last - first;
	integer = new int [size];

	if(size > 0)
	{
		for(unsigned int i = 0; i < size; i++)
			integer[i] = *(first + i);

		capacity = size;
	}
	else
	{
		size = 0;
		capacity = 0;
	}
}

MyVector::MyVector(const MyVector &x)
{
	size = x.size;
	capacity = x.capacity;
	integer = new int [x.size];

	if(size > 0)
	{
		for(unsigned int i = 0; i < size; i++)
			integer[i] = x.integer[i];
	}
}

// destructor
MyVector::~MyVector()
{
	delete [] integer;
} // end destructor

int* MyVector::begin()
{
	return integer;
}

int* MyVector::end()
{
	return integer + size;
}

int MyVector::getSize()
{
	return size;
}

void MyVector::resize( unsigned int n )
{
	unsigned int count = size - n;

	for(unsigned int i = 0; i < count; i++)
		pop_back();
}

void MyVector::resize( unsigned int n, const int val )
{
	size = n;
}

int MyVector::getCapacity()
{
	return capacity;
}

bool MyVector::empty()
{
	return ( size == 0 );
}

void MyVector::reserve( unsigned int n )
{
	if(size < n)
	{
		MyVector resize;

		resize.integer = new int [ n ];
		for(unsigned int i = 0; i < size; i++)
			resize.integer[i] = integer[i];

		resize.size = size;
		resize.capacity = n;

		this->integer = new int [n];
		for(unsigned int i = 0; i < size; i++)
			this->integer[i] = resize.integer[i];

		this->size = size;
		this->capacity = n;
	}
}

void MyVector::shrink_to_fit()
{
	if(capacity > size)
	{
		MyVector resize;

		resize.integer = new int [size];
		for(unsigned int i = 0; i < size; i++)
			resize.integer[i] = integer[i];

		resize.size = size;
		resize.capacity = size;

		this->integer = new int [resize.size];
		for(unsigned int i = 0; i < size; i++)
			this->integer[i] = resize.integer[i];

		this->size = resize.size;
		this->capacity = resize.capacity;
	}
}

int& MyVector::front()
{
	return integer[0];
}

int& MyVector::back()
{
	return integer[ size - 1 ];
}

void MyVector::assign( int n, const int val )
{
	for(unsigned int i = 0; i < n; i++)
		integer[i] = val;

	size = n;
}

void MyVector::assign( int *first, int *last )
{
	size = last - first;
	integer = new int [size];

	if(size > 0)
	{
		for(unsigned int i = 0; i < size; i++)
			integer[i] = *(first + i);

		capacity = size;
	}
	else
	{
		size = 0;
		capacity = 0;
	}

}

void MyVector::push_back( const int val )
{
	if(capacity > size)
	{
		integer[size] = val;
		size++;
	}
	else
	{
		MyVector resize;

		resize.integer = new int [size + 1];
		for(unsigned int i = 0; i < size; i++)
			resize.integer[i] = integer[i];

		resize.integer[size] = val;
		resize.size = size + 1;
		resize.capacity = size + 1;

		this->integer = new int [resize.size];
		for(unsigned int i = 0; i < resize.size; i++)
			this->integer[i] = resize.integer[i];

		this->size = resize.size;
		this->capacity = resize.capacity;
	}
}

void MyVector::pop_back()
{
	size--;
}

int* MyVector::insert( int *position, const int val )
{
	MyVector resize;
	int j;

	resize.integer = new int [size + 1];
	for(unsigned int i = 0; i < size + 1; i++)
	{
		if(i == position - &integer[0])
		{
			resize.integer[i] = val;
		}
		else if(i > position - &integer[0])
		{
			j = i - 1;
			resize.integer[i] = integer[j]; 
		}
		else
		{
		    resize.integer[i] = integer[i];
		}
	}

	resize.size = size + 1;
	resize.capacity = size + 1;

	this->integer = new int [resize.size];
	for(unsigned int i = 0; i < resize.size; i++)
		this->integer[i] = resize.integer[i];

	this->size = resize.size;
	this->capacity = resize.capacity;

	return integer;
}

int* MyVector::insert( int *position, unsigned int n, const int val  )
{
	MyVector resize;
	int j;

	resize.integer = new int [size + n];
	for(unsigned int i = 0; i < size + n; i++)
	{
		if(i >= position - &integer[0] && i < position - &integer[0] + 9)
		{
			resize.integer[i] = val;
		}
		else if(i >= position - &integer[0] + 9)
		{
            j = i - n;			
			resize.integer[i] = integer[j]; 
		}
		else
		{
		    resize.integer[i] = integer[i];
		}
	}

	resize.size = size + n;
	resize.capacity = size + n;

	this->integer = new int [resize.size];
	for(unsigned int i = 0; i < resize.size; i++)
		this->integer[i] = resize.integer[i];

	this->size = resize.size;
	this->capacity = resize.capacity;
	
	return integer;
}

int* MyVector::insert( int *position, int *first, int *last )
{
	MyVector resize;
	int j,val = *first;
	int range = last - first;
	
	resize.integer = new int [size + range];
	for(unsigned int i = 0; i < size + range; i++)
	{
		if(i >= position - &integer[0] && i < position - &integer[0] + range)
		{
			resize.integer[i] = val;
		}
		else if(i >= position - &integer[0] + range)
		{
            j = i - range;			
			resize.integer[i] = integer[j]; 
		}
		else
		{
		    resize.integer[i] = integer[i];
		}
	}

	resize.size = size + range;
	resize.capacity = size + range;

	this->integer = new int [resize.size];
	for(unsigned int i = 0; i < resize.size; i++)
		this->integer[i] = resize.integer[i];

	this->size = resize.size;
	this->capacity = resize.capacity;
	
	return integer;
}

int* MyVector::erase( int *position )
{
	for(unsigned int i = 0; i < size - 1; i++)
	{
		if(i >= position - &integer[0])
		{
			integer[i] = integer[i+1];
		}
	}

	size--;
	return integer;
}

int* MyVector::erase(  int *first, int *last )
{
	int range = last - first;

	for(unsigned int i = last - &integer[0]; i < size; i++)
		integer[i - range] = integer[i];

	size = size - range;
	return integer;
}

void MyVector::swap( MyVector &x )
{
	MyVector resize;

	resize.integer = new int [x.size];
	for(unsigned int i = 0; i < x.size; i++)
		resize.integer[i] = x.integer[i];

	resize.size = x.size;
	resize.capacity = x.capacity;

	int maxSize = (size > x.size) ? size : x.size;

	x.integer = new int [maxSize];
	for(unsigned int i = 0; i < size; i++)
		x.integer[i] = integer[i];
	x.size = size;
	x.capacity = maxSize;

	integer = new int [maxSize];
	for(unsigned int i = 0; i < resize.size; i++)
		integer[i] = resize.integer[i];

	size = resize.size;
	capacity = resize.capacity;
}

void MyVector::clear()
{
	size = 0;
}

void MyVector::display()
{
	for( unsigned int i = 0; i < size; i++ )
	{
		cout << setw(3) << integer[ i ];

		if( i % 10 == 9 || i == size - 1 )
			cout << endl;
	}
}