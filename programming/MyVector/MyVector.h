// MyVector class definition.
#ifndef MYVECTOR_H
#define MYVECTOR_H
using namespace std;

class MyVector 
{
public:
	static const unsigned int npos = -1;

	MyVector(); // Constructs an empty container, with no elements.
	MyVector( unsigned int n, const int val ); // Constructs a container with n elements. Each element is a copy of val.
	MyVector( int *first, int *last ); 
	MyVector( const MyVector &x ); // Constructs a container with a copy of each of the elements in x, in the same order.
	~MyVector(); // Destroys the container object.

	int* begin(); 
	int* end(); 
	int getSize(); // Returns the number of elements in the vector.
	void resize( unsigned int n ); 
	void resize( unsigned int n, const int val ); // The new elements are initialized as copies of val.
	int getCapacity(); // Returns the size of the storage space currently allocated for the vector, expressed in terms of elements.
	bool empty(); // Returns whether the vector is empty (i.e. whether its size is 0).
	void reserve( unsigned int n ); 
	void shrink_to_fit(); // Requests the container to reduce its capacity to fit its size.
	int& front(); 
	int& back(); 
	void assign( int n, const int val ); 
	void assign ( int *first, int *last ); 
	void push_back( const int val ); 
	void pop_back(); 
	int* insert( int *position, const int val ); 
	int* insert( int *position, unsigned int n, const int val ); 
	int* insert( int *position, int *first, int *last ); 
	int* erase( int *position );
	int* erase( int *first, int *last ); 
	void swap( MyVector &x ); 
	void clear(); 
	void display(); 

private:
	unsigned int size; 
	unsigned int capacity; 
	int *integer; 
}; // end class MyVector

#endif