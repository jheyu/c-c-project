// MyVector test program.
#include <iostream>
#include <iomanip>
#include <ctime>
#include "MyVector.h" // include definition of class MyVector
using namespace std;

void display( int intArray[] );
int enterChoice();

int main()
{
//   system("mode con:cols=60 lines=22");
   system( "color F0" );
//   srand( static_cast< unsigned int >( time(0) ) );

   unsigned int seed;
   cout << "Enter seed: ";
   cin >> seed;
   srand( seed );
   cout << endl;

   MyVector vector1;

   int intArray[ 30 ];
   for ( int i = 0; i < 30; i++ )
      intArray[ i ] = rand() % 100;
   cout << "intArray: " << endl;
   display( intArray );
   cout << endl;

   unsigned int first = rand() % 10;
   unsigned int last = 20 + rand() % 11;
   MyVector vector2( intArray + first, intArray + last );
   cout << "range: " << first << "-" << last - 1 << endl;
   cout << "vector2: " << endl;
   vector2.display();
   cout << endl;

   int value = rand() % 100;
   unsigned int size = 10 + rand() % 21;
   MyVector vector3( 10 + rand() % 21, value );
   cout << "vector3.size: " << vector3.getSize() << endl;
   vector3.display();
   cout << endl;

   MyVector vector4( vector2.begin() + 2, vector2.end() - 2 );
   cout << "vector4.size: " << vector4.getSize() << endl;
   vector4.display();
   cout << endl;

   MyVector vector5( vector2 );
   cout << "vector5.size: " << vector5.getSize() << endl;
   vector5.display();
   cout << endl;

   unsigned int newSize;
   unsigned int newCapacity;
   unsigned int position;
   unsigned int n;

   int choice; // store user choice
   // enable user to specify action
   while ( ( choice = enterChoice() ) != 12 ) 
   {
      switch ( choice ) 
      {
         case 1:
            vector1.assign( intArray + rand() % 11, intArray + 20 + rand() % 11 );
            cout << "Before resize: vector1.size = " << vector1.getSize() << endl;
            vector1.display();
            cout << endl;
			
            newSize = 10 + rand() % 21;
            vector1.resize( newSize );

            cout << "After resize: vector1.size = " << vector1.getSize() << endl;
            vector1.display();
            cout << endl;
            break;
         case 2:
            vector1.assign( intArray + rand() % 11, intArray + 20 + rand() % 11 );
            cout << "Before resize: vector1.size = " << vector1.getSize() << endl;
            vector1.display();
            cout << endl;
			
            newSize = 10 + rand() % 21;
            vector1.resize( newSize, vector1.front() );

            cout << "After resize: vector1.size = " << vector1.getSize() << endl;
            vector1.display();
            cout << endl;
            break;
         case 3:
            vector1.assign( intArray + rand() % 11, intArray + 20 + rand() % 11 );
            cout << "Before reserve: vector1.size = " << vector1.getSize() << endl;
            cout << "Before reserve: vector1.capacity = " << vector1.getCapacity() << endl;
            vector1.display();
            cout << endl;
			
            newCapacity = vector1.getCapacity() + 1 + rand() % 10;
			cout<<newCapacity<<endl;
            vector1.reserve( newCapacity );

            cout << "After reserve: vector1.size = " << vector1.getSize() << endl;
            cout << "After reserve: vector1.capacity = " << vector1.getCapacity() << endl;
            vector1.display();
            cout << endl;

            vector1.shrink_to_fit();

            cout << "After shrink_to_fit: vector1.size = " << vector1.getSize() << endl;
            cout << "After shrink_to_fit: vector1.capacity = " << vector1.getCapacity() << endl;
            vector1.display();
            cout << endl;
            break;
         case 4:
            vector1.assign( intArray + rand() % 11, intArray + 20 + rand() % 11 );
            cout << "Before assign: vector1.size = " << vector1.getSize() << endl;
            vector1.display();
            cout << endl;

            newSize = 10 + rand() % 21;
            value = vector1.back();
            vector1.assign( newSize, value );

            cout << "After assign: vector1.size = " << vector1.getSize() << endl;
            vector1.display();
            cout << endl;
            break;
         case 5:
            vector1.assign( intArray + rand() % 11, intArray + 20 + rand() % 11 );
            cout << "Before push_back: vector1.size = " << vector1.getSize() << endl;
            vector1.display();
            cout << endl;

            value = vector1.front();
            vector1.push_back( value );

            cout << "After push_back: vector1.size = " << vector1.getSize() << endl;
            vector1.display();
            cout << endl;
            break;
         case 6:
            vector1.assign( intArray + rand() % 11, intArray + 20 + rand() % 11 );
            cout << "Before insert: vector1.size = " << vector1.getSize() << endl;
            vector1.display();
            cout << endl;

            position = rand() % vector1.getSize();
            value = vector1.back();
            vector1.insert( vector1.begin() + position, value );
		
            cout << "position: " << position << endl;
            cout << "After insert: vector1.size = " << vector1.getSize() << endl;
            vector1.display();
            cout << endl;
            break;
         case 7:
            vector1.assign( vector2.begin(), vector2.end() );
            cout << "Before insert: vector1.size = " << vector1.getSize() << endl;
            vector1.display();
            cout << endl;

            position = rand() % vector1.getSize();
            n = 1 + rand() % 10;
            value = vector1.back();
            vector1.insert( vector1.begin() + position, n, value );

            cout << "position: " << position << endl;
            cout << "n: " << n << endl;
            cout << "After insert: vector1.size = " << vector1.getSize() << endl;
            vector1.display();
            cout << endl;
            break;
         case 8:
            vector1.assign( intArray + rand() % 11, intArray + 20 + rand() % 11 );
            cout << "Before insert: vector1.size = " << vector1.getSize() << endl;
            vector1.display();
            cout << endl;

            position = rand() % vector1.getSize();
            first = vector1.getSize() / 4;
            last = vector1.getSize() * 3 / 4;
            vector1.insert( vector1.begin() + position, vector3.begin() + first, vector3.begin() + last );

            cout << "position: " << position << endl;
            cout << "first = " << first << "   last = " << last << endl;
            cout << "After insert: vector1.size = " << vector1.getSize() << endl;
            vector1.display();
            cout << endl;
            break;
         case 9:
            vector1.assign( intArray + rand() % 11, intArray + 20 + rand() % 11 );
            cout << "Before erase: vector1.size = " << vector1.getSize() << endl;
            vector1.display();
            cout << endl;

            position = rand() % vector1.getSize();
            vector1.erase( vector1.begin() + position );

            cout << "position: " << position << endl;
            cout << "After erase: vector1.size = " << vector1.getSize() << endl;
            vector1.display();
            cout << endl;
            break;
         case 10:
            vector1.assign( intArray + rand() % 11, intArray + 20 + rand() % 11 );
            cout << "Before erase: vector1.size = " << vector1.getSize() << endl;
            vector1.display();
            cout << endl;

            first = vector1.getSize() / 4;
            last = vector1.getSize() * 3 / 4;
            vector1.erase( vector1.begin() + first, vector1.begin() + last );

            cout << "first = " << first << "   last = " << last << endl;
            cout << "After erase: vector1.size = " << vector1.getSize() << endl;
            vector1.display();
            cout << endl;
            break;
         case 11:
            cout << "Before swap: vector4.size = " << vector4.getSize() << endl;
            vector4.display();
            cout << endl;

            cout << "Before swap: vector5.size = " << vector5.getSize() << endl;
            vector5.display();
            cout << endl;

            vector5.swap( vector4 );

            cout << "After swap: vector4.size = " << vector4.getSize() << endl;
            vector4.display();
            cout << endl;

            cout << "After swap: vector5.size = " << vector5.getSize() << endl;
            vector5.display();
            cout << endl;
            break;
         default: // display error if user does not select valid choice
            cerr << "Incorrect choice" << endl;
            break;
      } // end switch
   } // end while

   system("pause");
} // end main

// enable user to input menu choice
int enterChoice()
{
   // display available options
   cout << "Enter your choice" << endl
      << "1 - resize( unsigned int n )" << endl
      << "2 - resize( unsigned int n, const int val )" << endl
      << "3 - reserve & shrink_to_fit" << endl
      << "4 - assign( int n, const int val )" << endl
      << "5 - push_back" << endl
      << "6 - insert( int *position, const int val )" << endl
      << "7 - insert( int *position, unsigned int n, const int val )" << endl
      << "8 - insert( int *position, int *first, int *last )" << endl
      << "9 - erase( int *position )" << endl
      << "10 - erase( int *first, int *last )" << endl
      << "11 - swap" << endl
      << "12 - end program\n? ";

   int menuChoice;
   cin >> menuChoice; // input menu selection from user
   cout << endl;
   return menuChoice;
} // end function enterChoice

void display( int intArray[] )
{
   for ( int i = 0; i < 30; i++ )
   {
      cout << setw(3) << intArray[ i ];
      if( i % 10 == 9 || i == 49 )
         cout << endl;
   }
}