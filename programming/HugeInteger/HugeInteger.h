 // HugeInteger class definition.
#ifndef HUGEINTEGER_H
#define HUGEINTEGER_H
using namespace std;

class HugeInteger 
{
public:
   HugeInteger(); // default constructor
   HugeInteger( int ); // constructor

   void random(); // randomly generate an integer
   bool zero(); // is zero
   bool equal( HugeInteger ); // is equal to
   bool notEqual( HugeInteger ); // not equal to
   bool greater( HugeInteger ); // greater than
   bool less( HugeInteger ); // less than
   bool greaterEqual( HugeInteger ); // greater than or equal to
   bool lessEqual( HugeInteger ); // less than or equal
   
   HugeInteger add( HugeInteger ); // addition operator; HugeInteger + HugeInteger
   HugeInteger subtract( HugeInteger ); // subtraction operator; HugeInteger - HugeInteger
   HugeInteger multiply( HugeInteger op2 ); // multiplication operator; HugeInteger * HugeInteger
   HugeInteger divide( HugeInteger op2 ); // division operator; HugeInteger / HugeInteger
   HugeInteger modulus( HugeInteger op2 ); // modulus operator; HugeInteger % HugeInteger

   void output(); // output
private:
   int size; // the number of digits of the integer
   short integer[ 1000 ]; // 1000 element array
   void divideByTen();
}; // end class HugeInteger

#endif
