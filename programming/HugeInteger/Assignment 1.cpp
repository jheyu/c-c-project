// HugeInteger test program.
#include <iostream>
#include <ctime>
#include "HugeInteger.h" // include definition of class HugeInteger
using namespace std;

int main()
{
   int seed;
   cout << "Enter seed: ";
   cin >> seed;
   srand( seed );
   cout << endl;

   HugeInteger n1( 20 ); // HugeInteger object n1
   // tests for zero at n1
   if ( n1.zero() )
   {
      cout << "n1 contains ";
      n1.output();
      cout << "\n\n";  
   } // end if statement

   n1.random();
   // checks for equality between n1 and n1 
   if ( n1.equal( n1 ) )
   { 
      n1.output(); 
      cout << " is equal to ";
      n1.output(); 
      cout << "\n\n"; 
   } // end if

   HugeInteger n2( 20 ); // HugeInteger object n2
   n2.random();
   // checks for equality between n1 and n2 
   if ( n1.equal( n2 ) )
   { 
      n1.output(); 
      cout << " is equal to ";
      n2.output(); 
      cout << "\n\n"; 
   } // end if

   // checks for inequality between n1 and n2
   if ( n1.notEqual( n2 ) )
   {
      n1.output(); 
      cout << " is not equal to ";
      n2.output(); 
      cout << "\n\n";  
   } // end if 

   // tests for greater number between n1 and n2 
   if ( n1.greater( n2 ) )
   {
      n1.output(); 
      cout << " is greater than ";
      n2.output(); 
      cout << "\n\n";  
   } // end if 

   // tests for greater number between n2 and n1 
   if ( n2.greater( n1 ) )
   {
      n2.output(); 
      cout << " is greater than ";
      n1.output(); 
      cout << "\n\n";  
   } // end if 

   // tests for smaller number between n1 and n2
   if ( n1.less( n2 ) )
   {
      n1.output(); 
      cout << " is less than ";
      n2.output(); 
      cout << "\n\n";  
   } // end if 

   // tests for smaller number between n2 and n1
   if ( n2.less( n1 ) )
   {
      n2.output(); 
      cout << " is less than ";
      n1.output(); 
      cout << "\n\n";  
   } // end if 
    
   // tests for smaller or equal number between n1 and n1
   if ( n1.lessEqual( n1 ) )
   {
      n1.output(); 
      cout << " is less than or equal to ";
      n1.output(); 
      cout << "\n\n";  
   } // end if 

   // tests for smaller or equal number between n1 and n2
   if ( n1.lessEqual( n2 ) )
   {
      n1.output(); 
      cout << " is less than or equal to ";
      n2.output(); 
      cout << "\n\n";  
   } // end if 

   // tests for greater or equal number between n2 and n2
   if ( n2.greaterEqual( n2 ) )
   {
      n2.output(); 
      cout << " is greater than or equal to ";
      n2.output(); 
      cout << "\n\n";  
   } // end if 
    
   // tests for greater or equal number between n1 and n2
   if ( n1.greaterEqual( n2 ) )
   {
      n1.output(); 
      cout << " is greater than or equal to ";
      n2.output(); 
      cout << "\n\n";  
   } // end if 
    
   cout << "----------------------------------------------------------------------" << "\n\n"; 

	HugeInteger n3( 12 ); // HugeInteger object n3
   HugeInteger n4( 7 ); // HugeInteger object n4

   n3.random();
   n4.random();


   
   // assigns the result of ( n3 / n4 ) to n5 then outputs n5
   HugeInteger n5; // HugeInteger object n5   
   n5 = n3.divide( n4 );
   n3.output();
   cout<< " / ";
   n4.output();
   cout << " = ";
   n5.output();
   cout << "\n\n";
   
   // outputs the result of ( n4 * n5 ) to n6 then outputs n6
   HugeInteger n6; // HugeInteger object n6
   n6 = n4.multiply( n5 );  
   n4.output();               
   cout << " * ";         
   n5.output(); 
   cout << " = "; 
   n6.output();
   cout << "\n\n";

   // assigns the result of ( n3 - n6 ) to n7 then outputs n7
   HugeInteger n7; // HugeInteger object n7
   n7 = n3.subtract( n6 );
   n3.output();
   cout<< " - ";
   n6.output();
   cout << " = ";
   n7.output();
   cout << "\n\n";

   // assigns the result of ( n3 % n4 ) to n8 then outputs n8
   HugeInteger n8; // HugeInteger object n8
   n8 = n3.modulus( n4 );
   n3.output();
   cout<< " % ";
   n4.output();
   cout << " = ";
   n8.output();
   cout << "\n\n";

   // the result of ( n8 + n6 ) to n9 then outputs n9
   HugeInteger n9; // HugeInteger object n9
   n9 = n8.add( n6 );  
   n8.output();
   cout << " + ";
   n6.output();
   cout << " = "; 
   n9.output();
   cout << "\n\n";

   // checks for equality between n3 and n9 
   if ( n3.equal( n9 ) )
   { 
      n3.output(); 
      cout << " = ";
      n9.output(); 
      cout << "\n\n"; 
   } // end if
   

	system("pause");
} // end main