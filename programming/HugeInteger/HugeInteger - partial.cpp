// Member-function definitions for class HugeInteger.
#include <iostream>
#include "HugeInteger.h" // include definition of class HugeInteger 
using namespace std;

// default constructor
HugeInteger::HugeInteger()
{
   size = 1000;
   // initialize array to zero
   for ( int i = 0; i < size; i++ )
      integer[ i ] = 0;
} // end HugeInteger constructor

// constructor
HugeInteger::HugeInteger( int s )
{
   size = ( s > 0 && s <= 1000 ) ? s : 0;
   // initialize array to zero
   for ( int i = 0; i < size; i++ )
      integer[ i ] = 0;
} // end HugeInteger constructor

// randomly generate an integer
void HugeInteger::random()
{
   integer[ size - 1 ] = 1 + rand() % 9;
   for ( int i = 0; i < size - 1; i++ )
      integer[ i ] = rand() % 10;
} // end Random

// function that tests if a HugeInteger is zero
bool HugeInteger::zero()
{
   if( size == 0 )
      return true;

   for ( int i = 0; i < size; i++ )
      if ( integer[ i ] != 0 )
         return false;
         
   return true;
} // end function zero
bool HugeInteger::equal( HugeInteger op2)
{
	int i;
	if( size == op2.size)
      {
	     for( i = 0; i < size; i++ )
	         if( integer[ i ] != op2.integer[ i ] )
				 return false;
		 return true;
	  }
	else 
		return false;

}

bool HugeInteger::notEqual( HugeInteger op2)
{
	int i;
	if( size == op2.size)
      {
	     for( i = 0; i < size; i++ )
	         if( integer[ i ] != op2.integer[ i ] )
				 return true;
		 return false;
	  }
	else 
		return true;


}

bool HugeInteger::greater( HugeInteger op2)
{  
	int i;

	if( size > op2.size)
		return true;
	else if( size < op2.size )
		return false;
	else

	for( i = 0; i < size; i++ )
	   {
		if( integer[ size - i - 1 ] > op2.integer[ size - i - 1 ] )
			return true;

		else if( integer[ size - i - 1 ] < op2.integer[ size - i - 1 ] )
			return false;
	   }
}

bool HugeInteger::less( HugeInteger op2)
{
	int i;
	if( size > op2.size )
	   return false;
	
	else if( size < op2.size )
		return true;

	else
		for( i = 0; i < size; i++ )
		   {
			if( integer[ size - i - 1 ] > op2.integer[ size - i - 1 ] )
				return false;

			else if( integer[ size - i - 1 ] < op2.integer[ size - i - 1 ] )
				return true;
		   }




}
bool HugeInteger::greaterEqual( HugeInteger op2)
{
	int i;
	if( size > op2.size )
	   return true;
	else if( size < op2.size )
		return false;
	else
	{

	for( i = 0; i < size; i++ )
	   {
		if( integer[ size - i - 1 ] > op2.integer[ size - i - 1 ] )
			return true;

		else if( integer[ size - i - 1 ] < op2.integer[ size - i - 1 ] )
			return false;

	   }
	if( i == size )
			return true;
	}
}

bool HugeInteger::lessEqual( HugeInteger op2)
{
	int i;
	if( size > op2.size )
	   return false;
	else if( size < op2.size )
		return true;
	else
	{

	for( i = 0; i < size; i++ )
	   {
		if( integer[ size - i - 1 ] > op2.integer[ size - i - 1 ] )
			return false;

		else if( integer[ size - i - 1 ] < op2.integer[ size - i - 1 ] )
			return true;
		
	   }

	if( i == size )
			return true;
	}
}

// addition operator; HugeInteger + HugeInteger
HugeInteger HugeInteger::add( HugeInteger op2 )
{
   int sumSize = ( size >= op2.size ) ? size : op2.size;
   HugeInteger sum( sumSize ); // temporary result

   if( size <= op2.size )
   {
      // iterate through HugeInteger
      for ( int i = 0; i < size; i++ )
         sum.integer[ i ] = integer[ i ] + op2.integer[ i ];

      for ( int i = size; i < sum.size; i++ )
         sum.integer[ i ] = op2.integer[ i ];
   }
   else
   {
      // iterate through HugeInteger
      for ( int i = 0; i < op2.size; i++ )
         sum.integer[ i ] = integer[ i ] + op2.integer[ i ];

      for ( int i = op2.size; i < sum.size; i++ )
         sum.integer[ i ] = integer[ i ];
   }

   for ( int i = 0; i < sum.size - 1; i++ ) 
   {
      // determine whether to carry a 1
      if ( sum.integer[ i ] > 9 ) 
      {
         sum.integer[ i ] -= 10; // reduce to 0-9
         sum.integer[ i + 1 ]++;
      } // end if
   } // end for

	if( sum.integer[ sum.size ] > 0 )
		sum.size++;

   return sum; // return the sum
} // end function add

// overloaded output operator
void HugeInteger::output()
{
   int k;
   if( size == 0 )
      cout << 0;
   else
   {
      for( k = size - 1; k >= 0; k-- )
         if( integer[ k ] != 0 )
            break;

      if( k == -1 )
         cout << 0;
      else
         for( int i = k; i >= 0; i-- ) // display the HugeInteger
            cout << integer[ i ];
   }
} // end function output

void HugeInteger::divideByTen()
{
   for( int i = 1; i < size; i++ )
      integer[ i - 1 ] = integer[ i ];
   integer[ size ] = 0;
   size--;
}

HugeInteger HugeInteger::multiply(HugeInteger op2)
{
	int i,j;
	int multSize = size + op2.size ;
   HugeInteger mult( multSize ); // temporary result

   for( i = 0 ; i < op2.size ; i++ )
      {
	   for( j = 0 ; j < size ; j++ )
		 mult.integer[ i + j ] = mult.integer[ i + j ] + integer[ j ] * op2.integer[ i ];
      }
   for( i = 0 ; i < multSize ; i++ )
      {
         mult.integer[ i + 1 ] += ( mult.integer[ i ] / 10 );
         mult.integer[ i ] %= 10;
      }
   if( mult.integer[ mult.size - 1 ] == 0 )
      mult.size--;

   return mult;
 
}

HugeInteger HugeInteger::subtract(HugeInteger op2)
{
	int subSize = ( size >= op2.size ) ? size : op2.size;
    HugeInteger sub( subSize ); // temporary result
	int i;
   
	


   if( this->less(op2) )
   {
      // iterate through HugeInteger
      for ( i = 0; i < size; i++ )
	      sub.integer[ i ] = op2.integer[ i ] - integer[ i ];
	     
      
	  for ( i = size; i < subSize; i++ )
         sub.integer[ i ] = op2.integer[ i ];

	}
   else
   {
     		  
	   // iterate through HugeInteger
      for ( i = 0; i < op2.size; i++ )
		  sub.integer[ i ] = integer[ i ] - op2.integer[ i ];
	     
      
	  for ( i = op2.size; i < subSize; i++ ){
          sub.integer[ i ] = integer[ i ];

	  }

   }

   for ( i = 0; i < subSize - 1; i++ ){
	    if( sub.integer[ i ] < 0 )
		  { 
			  sub.integer[ i + 1 ]--;
	          sub.integer[ i ] += 10;

			  
		  }
		
   }
   
   while( sub.integer[ sub.size - 1 ] == 0 )
		sub.size--;

   

  

   return sub; // return the sub



}

HugeInteger HugeInteger::modulus(HugeInteger op2)
{
    int quoSize = size - op2.size + 1;
    HugeInteger quo( quoSize ); // temporary result
	HugeInteger remainder( size );
	HugeInteger buffer = op2;
	int i;
	/*
	for( i = op2.size - 1 ; i >= 0; i--)
	    printf("%d",buffer.integer[ i ]);
	printf("\n");
    printf("%d",buffer.size);
	printf("-------------------------------");
	*/
	for(i = 0; i < size; i++ )//integer 存入remainder
	    remainder.integer[ i ] = integer[ i ];
   

	if( remainder.greater( buffer ) )
	    {
		    for( i = 0; i < buffer.size; i++ )//size > op2.size ,則op2.size移位,並補零
	           buffer.integer[ size - i - 1 ] = buffer.integer[ buffer.size - i - 1 ];
	        for( i = 0; i < size - buffer.size; i++ )
	           buffer.integer[ i ] = 0;

	       buffer.size = size;
			for( i = 0; i < quoSize ; i++ ) 
		    {
			  while( remainder.greater( buffer ) )
			   { 
                 remainder = remainder.subtract( buffer );
		         quo.integer[ quoSize - i - 1 ]+=1; 
               } 
			   if( remainder.integer[ remainder.size - 1 ] == 0 )
			     remainder.size--;
			   buffer.divideByTen();
			   
			}
		   
	        
         }
	 else         
		 quo.size = 0;
			
     if( quo.integer[ quo.size - 1 ] == 0 )
		  quo.size--; 
    
	 return remainder;
}

HugeInteger HugeInteger::divide(HugeInteger op2)
{
	int quoSize = size - op2.size + 1;
    HugeInteger quo( quoSize ); // temporary result
	HugeInteger remainder( size );
	HugeInteger buffer = op2;
	int i;
	
	for(i = 0; i < size; i++ )//integer 存入remainder
	    remainder.integer[ i ] = integer[ i ];
	
	if( remainder.greater( buffer ) )
	    {
		    for( i = 0; i < buffer.size; i++ )//size > op2.size ,則op2.size移位,並補零
	           buffer.integer[ size - i - 1 ] = buffer.integer[ buffer.size - i - 1 ];
	        for( i = 0; i < size - buffer.size; i++ )
	           buffer.integer[ i ] = 0;
			
	        buffer.size = size;
			for( i = 0; i < quoSize ; i++ ) 
		    {

			  while( remainder.greater( buffer ) )
			   { 
                 remainder = remainder.subtract( buffer );
		         quo.integer[ quoSize - i - 1 ]+=1; 
               } 
			   if( remainder.integer[ remainder.size - 1 ] == 0 )
			     remainder.size--;
			   buffer.divideByTen();

			}
		   
	        
        }
	 else         
		 quo.size = 0;
		
	 if( quo.integer[ quo.size - 1 ] == 0 )
		  quo.size--; 
	 
	

	 return quo;
}
