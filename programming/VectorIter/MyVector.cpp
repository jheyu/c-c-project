// Member-function definitions for class MyVector.
#include <iostream>
#include <iomanip>
#include "MyVector.h" // include definition of class MyVector 
using namespace std;

// default constructor
template< typename T >
MyVector< T >::MyVector( unsigned int n, const T val )
{
	if( n > 0 )
	{
		ptr = new T[ n ];
		// initialize array to val
		for ( unsigned int i = 0; i < n; i++ )
			ptr[ i ] = val;

		size = n;
		capacity = n;
	}
	else
	{
		size = 0;
		capacity = 0;
		ptr = new T[ 0 ];
	}
} // end MyVector constructor

// constructor; 
template< typename T >
MyVector< T >::MyVector( VectorIter< T > first, VectorIter< T > last )
{
	// place digits of argument into array
	size = last - first;
	capacity = size;

	ptr = new T[ capacity ];

	for ( unsigned int i = 0; i < size; i++ )
		ptr[ i ] = *( first + i );
} // end MyVector constructor

// copy constructor
template< typename T >
MyVector< T >::MyVector( const MyVector< T > &vectorToCopy )
{
	size = vectorToCopy.size;
	capacity = vectorToCopy.capacity;

	ptr = new T[ capacity ];

	for ( unsigned int i = 0; i < size; i++ )
		ptr[ i ] = vectorToCopy.ptr[ i ];
} // end MyVector constructor

// destructor
template< typename T >
MyVector< T >::~MyVector()
{
	delete [] ptr;
} // end destructor

/*
template< typename T >
const MyVector< T > &MyVector< T >::operator=( const MyVector< T > &right )
{
}

template< typename T >
bool MyVector< T >::operator==( const MyVector< T > &right ) const
{
}

template< typename T >
bool MyVector< T >::operator!=( const MyVector< T > &right ) const
{
}
*/

template < typename T >
ostream& operator<<( ostream &os, const MyVector< T > &right)
{
	os << *right;
	return os;
}

template < typename T >
T &MyVector< T >::operator[]( unsigned int subscript)
{
	if(subscript >= 0 && subscript < size)
		return ptr[subscript];
}

template < typename T >
T MyVector< T >::operator[]( unsigned int subscript) const
{
	if(subscript >= 0 && subscript < size)
		return ptr[subscript];
}

template < typename T >
VectorIter< T > MyVector< T >::begin()
{
	return ptr;
}

template< typename T >
VectorIter< T > MyVector< T >::end()
{
	return (ptr + size);
}

template< typename T >
unsigned int MyVector< T >::getSize() const 
{
	return size;
}

template< typename T >
void MyVector< T >::resize( unsigned int n )
{
	MyVector< T > temp(n,0);
	if(size > n)
	{
		size = n;
		capacity = n;
	}
	else if(size < n)
	{
		for(unsigned int i = 0; i < size; i++)
			temp.ptr[i] = ptr[i];
		ptr = new int [n];
		for(unsigned int i = 0; i < size; i++)
			ptr[i] = temp.ptr[i];
		for(unsigned int i = size; i < n; i++)
			ptr[i] = 0;
		size = n;
		capacity = n;
	}
}


template< typename T >
void MyVector< T >::resize( unsigned int n, const T val )
{
	MyVector< T > temp(n,0);
	if(size > n)
	{
		size = n;
		capacity = n;
	}
	else if(size < n)
	{
		for(unsigned int i = 0; i < size; i++)
			temp.ptr[i] = ptr[i];
		ptr = new int [n];
		for(unsigned int i = 0; i < size; i++)
			ptr[i] = temp.ptr[i];
		for(unsigned int i = size; i < n; i++)
			ptr[i] = val;
		size = n;
		capacity = n;
	}
}

template< typename T >
unsigned int MyVector< T >::getCapacity() const
{
	return capacity;
}
/*
template < typename T >
bool MyVector< T >::empty() const
{
}
*/
template < typename T >
void MyVector< T >::reserve( unsigned int n )
{
	if(size < n)
	{
		MyVector resize(n,0);

		resize.ptr = new int [ n ];
		for(unsigned int i = 0; i < size; i++)
			resize.ptr[i] = ptr[i];

		resize.size = size;
		resize.capacity = n;

		ptr = new int [n];
		for(unsigned int i = 0; i < size; i++)
			ptr[i] = resize.ptr[i];

		size = size;
		capacity = n;
	}
}

template < typename T >
void MyVector< T >:: shrink_to_fit()
{
	if(capacity > size)
	{
		MyVector resize(size,0);

		resize.ptr = new int [size];
		for(unsigned int i = 0; i < size; i++)
			resize.ptr[i] = ptr[i];

		resize.size = size;
		resize.capacity = size;

		ptr = new int [resize.size];
		for(unsigned int i = 0; i < size; i++)
			ptr[i] = resize.ptr[i];

		size = resize.size;
		capacity = resize.capacity;
	}
}

template < typename T >
T& MyVector< T >::front()
{
	return ptr[0];
}


template < typename T >
T &MyVector< T >::back()
{
	return ptr[size - 1];
}

template < typename T >
void MyVector< T >::assign( unsigned int n, const T val )
{
	ptr = new int [n];
	for(unsigned int  i = 0; i < n; i++)
		ptr[i] = val;
	size = n;
	capacity = n;
}

template < typename T >
void MyVector< T >::assign( VectorIter< T > first, VectorIter< T > last )
{
	size = last - first;
	ptr = new int [size];

	if(size > 0)
	{
		for(unsigned int i = 0; i < size; i++)
			ptr[i] = *(first + i);
		capacity = size;
	}
	else
	{
		size = 0;
		capacity = 0;
	}
}

template < typename T >
void MyVector< T >::push_back( const T val )
{
	if(capacity > size)
	{
		ptr[size] = val;
		size++;
	}
	else
	{
		MyVector< T > resize(size+1,0);
		for(unsigned int i = 0; i < size; i++)
			resize.ptr[i] = ptr[i];
		resize.ptr[size] = val;
		resize.capacity = size + 1;

		ptr = new int [resize.size];
		for(unsigned int i = 0; i < resize.size; i++)
			ptr[i] = resize.ptr[i];

		size = resize.size;
		capacity = resize.capacity;
	}
}

template < typename T >
void MyVector< T >::pop_back()
{
	size--;
}

template < typename T >
VectorIter< T > MyVector< T >::insert( VectorIter< T > position, const T val )
{
	MyVector< T > resize(size + 1,0);
	int j;
	for(unsigned int i = 0; i < resize.size; i++)
	{
		if(i == position - this->begin())
			resize.ptr[i] = val;
		else if(i > position - this->begin())
		{
			j = i - 1;
			resize.ptr[i] = ptr[j];
		}
		else
		    resize.ptr[i] = ptr[i];
	}

	ptr = new int [resize.size];
	for(unsigned int i = 0; i < resize.size; i++)
		ptr[i] = resize.ptr[i];

	size = resize.size;
	capacity = resize.capacity;

	return ptr;
}

/*
template < typename T >
VectorIter< T > MyVector< T >::insert( VectorIter< T > position, unsigned int n, const T val )
{
}

template < typename T >
VectorIter< T > MyVector< T >::insert( VectorIter< T > position, VectorIter< T > first, VectorIter< T > last )
{
}

template < typename T >
VectorIter< T > MyVector< T >::erase( VectorIter< T > position )
{
}

template < typename T >
VectorIter< T > MyVector< T >::erase( VectorIter< T > first, VectorIter< T > last )
{
}

template < typename T >
void MyVector< T >::swap( MyVector< T > &x )
{

}

template < typename T >
void MyVector< T >::clear()
{
}
*/
template < typename T >
void MyVector< T >::display()
{
	for(unsigned int i = 0; i < size; i++)
	{
		cout << setw(3) << ptr[i];

		if(i % 10 == 9 || i == size - 1)
			cout << endl;
	}
}
