// Member-function definitions for class VectorIter.
#include <iostream>
#include <iomanip>
#include "VectorIter.h" // include definition of class VectorIter
using namespace std;

// default constructor
template< typename T >
VectorIter< T >::VectorIter( T *p )
   : ptr( p )
{
}

// overloaded dereference operator
template< typename T >
T& VectorIter< T >::operator*() const
{
   return *ptr;
}

template< typename T >
T* VectorIter< T >::operator->() const
{
   return ptr;
}

template < typename T >
VectorIter< T >& VectorIter< T >::operator++()
{
	VectorIter< T >temp(*this);//要傳她�媕Y的地址進去
	ptr++;
	return temp;
}

template < typename T >
VectorIter< T > VectorIter< T >::operator++( int )
{
	ptr++;
	return ptr;
}

template < typename T >
VectorIter< T > VectorIter< T >::operator+( int i )
{
	return (ptr + i);
}

template < typename T >
VectorIter< T > VectorIter< T >::operator-( int i )
{
	return (ptr - i);
}

template < typename T >
unsigned int VectorIter< T >::operator-( VectorIter< T > &right ) const
{
	return (ptr - right.ptr);
}

template < typename T >
bool VectorIter< T >::operator==( const VectorIter< T > &right ) const
{
	if(*ptr == *right)
		return true;
	else
		return false;
}

template < typename T >
bool VectorIter< T >::operator!=( const VectorIter< T > &right ) const
{
	if(*ptr != *right)
		return true;
	else
		return false;
}

template < typename T >
bool VectorIter< T >::operator<( const VectorIter< T > &right ) const
{
	if(*ptr < *right)
		return true;
	else
		return false;
}

template < typename T >
bool VectorIter< T >::operator<=( const VectorIter< T > &right ) const
{
	if(*ptr <= *right)
		return true;
	else
		return false;
}

template < typename T >
ostream& operator<<( ostream &os, const VectorIter< T > &right)
{
	os << *right;
	return os;
}
