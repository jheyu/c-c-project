// VectorIter class definition.
#ifndef INTVECITER_H
#define INTVECITER_H

#include <iostream>
using namespace std;

template< typename T >
class VectorIter 
{
   friend ostream &operator<<( ostream &, const VectorIter &right );
public:
   VectorIter( T *p = 0 );
   T& operator*() const;
   T* operator->() const;
   VectorIter< T >& operator++();
   VectorIter< T > operator++( int );
   VectorIter< T > operator+( int i );
   VectorIter< T > operator-( int i );
   unsigned int operator-( VectorIter< T > &right ) const;
   bool operator==( const VectorIter< T > &right ) const;
   bool operator!=( const VectorIter< T > &right ) const;
   bool operator<( const VectorIter< T > &right ) const;
   bool operator<=( const VectorIter< T > &right ) const;
private:
   T *ptr; // keep a pointer to MyVector
}; // end class VectorIter

#endif