// MyVector test program.
#include <iostream>
#include <iomanip>
#include <ctime>
#include "VectorIter.cpp" // include definition of class template VectorIter
#include "MyVector.cpp" // include definition of class template MyVector
using namespace std;

void display( int intArray[] );
int enterChoice();

int main()
{
   unsigned int seed;

   cout << "Enter seed: ";
   cin >> seed;
   srand( seed );
   cout << endl;

   int intArray[ 30 ];
   for( unsigned int i = 0; i < 30; i++ )
      intArray[ i ] = rand() % 100;
   cout << "intArray: " << endl;
   display( intArray );
   cout << endl;
   
   unsigned int first = rand() % 10;
   unsigned int last = 20 + rand() % 11;
   MyVector< int >::iterator begin1( intArray + first );
   MyVector< int >::iterator end1( intArray + last );
   MyVector< int > vector1( begin1, end1 );
   cout << "range: " << first << "-" << last - 1 << endl;
   cout << "vector1: " << endl;
   vector1.display(); 
   cout << endl;
   vector1[0] = 101;
   vector1.display();
   cout << vector1[0] << endl;
   cout << endl;

   MyVector< int > vector10( begin1, end1 );
   MyVector< int >::iterator begin10( intArray + first );
   vector10.display();
   cout << endl;

   if(begin10 == begin1)
   {
	   cout << "vector1.begin():" << *begin1 << endl;
	   cout << "vector10.begin():" << *begin10 << endl;
	   begin1++;
	   cout << *begin1 << endl;
	   *begin1 = 10;
	   //*(begin1++) = 10;
	   cout << *begin1 << endl;
	   vector1.display();
	   cout << "Equal" << endl;
   }
   else
   {
	   cout << "Not Equal" << endl;
   }
   system("pause");

   int value = rand() % 100;
   MyVector< int > vector2( 10 + rand() % 21, value );
   cout << "vector2.size: " << vector2.getSize() << endl;
   vector2.display();
   cout << endl;
   
   MyVector< int >::iterator begin2( vector1.begin() );
   MyVector< int >::iterator end2( vector1.end() );
   MyVector< int > vector3( begin2 + 2, end2 - 2 );
   cout << "vector3.size: " << vector3.getSize() << endl;
   vector3.display();
   cout << endl;
   
   MyVector< int > vector4( vector1 );
   cout << "vector4.size: " << vector4.getSize() << endl;
   vector4.display();
   cout << endl;

   unsigned int newSize;
   unsigned int newCapacity;
   unsigned int position;
   unsigned int n;
   MyVector< int >::iterator begin3( intArray + rand() % 11 );
   MyVector< int >::iterator end3( intArray + 20 + rand() % 11 );
   
   MyVector< int > vector5;

   int choice; // store user choice
   
   while ( ( choice = enterChoice() ) != 12 ) 
   {
      vector5.assign( begin3, end3 );
      switch ( choice ) 
      {
         case 1:
            cout << "Before resize: vector5.size = " << vector5.getSize() << endl;
            vector5.display();
            cout << endl;
	
            newSize = 10 + rand() % 21;
            vector5.resize( newSize );

            cout << "After resize: vector5.size = " << vector5.getSize() << endl;
            vector5.display();
            cout << endl;
            break;
         case 2:
            cout << "Before resize: vector5.size = " << vector5.getSize() << endl;
            vector5.display();
            cout << endl;

            newSize = 10 + rand() % 21;
            vector5.resize( newSize, vector5.front() );

            cout << "After resize: vector5.size = " << vector5.getSize() << endl;
            vector5.display();
            cout << endl;
            break;
         case 3:
            cout << "Before reserve: vector5.size = " << vector5.getSize() << endl;
            cout << "Before reserve: vector5.capacity = " << vector5.getCapacity() << endl;
            vector5.display();
            cout << endl;

            newCapacity = vector5.getCapacity() + 1 + rand() % 10;
            vector5.reserve( newCapacity );

            cout << "After reserve: vector5.size = " << vector5.getSize() << endl;
            cout << "After reserve: vector5.capacity = " << vector5.getCapacity() << endl;
            vector5.display();
            cout << endl;
			
            vector5.shrink_to_fit();

            cout << "After shrink_to_fit: vector5.size = " << vector5.getSize() << endl;
            cout << "After shrink_to_fit: vector5.capacity = " << vector5.getCapacity() << endl;
            vector5.display();
            cout << endl;
            break;
         case 4:
            cout << "Before assign: vector5.size = " << vector5.getSize() << endl;
            vector5.display();
            cout << endl;

            newSize = 10 + rand() % 21;
            value = vector5.back();
            vector5.assign( newSize, value );

            cout << "After assign: vector5.size = " << vector5.getSize() << endl;
            vector5.display();
            cout << endl;
            break;
         case 5:
            cout << "Before push_back: vector5.size = " << vector5.getSize() << endl;
            vector5.display();
            cout << endl;

            value = vector5.front();
            vector5.push_back( value );

            cout << "After push_back: vector5.size = " << vector5.getSize() << endl;
            vector5.display();
            cout << endl;
            break;
         case 6:
            cout << "Before insert: vector5.size = " << vector5.getSize() << endl;
            vector5.display();
            cout << endl;

            position = rand() % vector5.getSize();
            value = vector5.back();
            vector5.insert( vector5.begin() + position, value );

            cout << "position: " << position << endl;
            cout << "After insert: vector5.size = " << vector5.getSize() << endl;
            vector5.display();
            cout << endl;
            break;/*
         case 7:
            cout << "Before insert: vector5.size = " << vector5.getSize() << endl;
            vector5.display();
            cout << endl;

            position = rand() % vector5.getSize();
            n = 1 + rand() % 10;
            value = vector5.back();
            vector5.insert( vector5.begin() + position, n, value );

            cout << "position: " << position << endl;
            cout << "n: " << n << endl;
            cout << "After insert: vector5.size = " << vector5.getSize() << endl;
            vector5.display();
            cout << endl;
            break;
         case 8:
            cout << "Before insert: vector5.size = " << vector5.getSize() << endl;
            vector5.display();
            cout << endl;

            position = rand() % vector5.getSize();
            first = vector5.getSize() / 4;
            last = vector5.getSize() * 3 / 4;
            vector5.insert( vector5.begin() + position, vector2.begin() + first, vector2.begin() + last );

            cout << "position: " << position << endl;
            cout << "first = " << first << "   last = " << last << endl;
            cout << "After insert: vector5.size = " << vector5.getSize() << endl;
            vector5.display();
            cout << endl;
            break;
         case 9:
            cout << "Before erase: vector5.size = " << vector5.getSize() << endl;
            vector5.display();
            cout << endl;

            position = rand() % vector5.getSize();
            vector5.erase( vector5.begin() + position );

            cout << "position: " << position << endl;
            cout << "After erase: vector5.size = " << vector5.getSize() << endl;
            vector5.display();
            cout << endl;
            break;
         case 10:
            vector5.assign( intArray + rand() % 11, intArray + 20 + rand() % 11 );
            cout << "Before erase: vector5.size = " << vector5.getSize() << endl;
            vector5.display();
            cout << endl;

            first = vector5.getSize() / 4;
            last = vector5.getSize() * 3 / 4;
            vector5.erase( vector5.begin() + first, vector5.begin() + last );

            cout << "first = " << first << "   last = " << last << endl;
            cout << "After erase: vector5.size = " << vector5.getSize() << endl;
            vector5.display();
            cout << endl;
            break;
         case 11:
            cout << "Before swap: vector3.size = " << vector3.getSize() << endl;
            vector3.display();
            cout << endl;

            cout << "Before swap: vector4.size = " << vector4.getSize() << endl;
            vector4.display();
            cout << endl;

            vector4.swap( vector3 );

            cout << "After swap: vector3.size = " << vector3.getSize() << endl;
            vector3.display();
            cout << endl;

            cout << "After swap: vector4.size = " << vector4.getSize() << endl;
            vector4.display();
            cout << endl;
            break;*/
         default: // display error if user does not select valid choice
            cerr << "Incorrect choice" << endl;
            break;
      } // end switch
   } // end while
   
   system("pause");
} // end main

// enable user to input menu choice
int enterChoice()
{
   // display available options
   cout << "Enter your choice" << endl
      << "1 - resize( unsigned int n )" << endl
      << "2 - resize( unsigned int n, const T val )" << endl
      << "3 - reserve & shrink_to_fit" << endl
      << "4 - assign( int n, const T val )" << endl
      << "5 - push_back" << endl
      << "6 - insert( T *position, const T val )" << endl
      << "7 - insert( T *position, unsigned int n, const T val )" << endl
      << "8 - insert( T *position, T *first, T *last )" << endl
      << "9 - erase( T *position )" << endl
      << "10 - erase( T *first, T *last )" << endl
      << "11 - swap" << endl
      << "12 - end program\n? ";

   int menuChoice;
   cin >> menuChoice; // input menu selection from user
   cout << endl;
   return menuChoice;
} // end function enterChoice

void display( int intArray[] )
{
   for( int i = 0; i < 30; i++ )
   {
      cout << setw(3) << intArray[ i ];
      if( i % 10 == 9 || i == 49 )
         cout << endl;
   }
}