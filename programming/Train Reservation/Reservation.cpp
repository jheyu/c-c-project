// Reservation.cpp
// Member-function definitions for class Reservation.
#include <iostream>
#include <iomanip>
#include "Reservation.h" // Reservation class definition
using namespace std;

// Reservation default constructor initializes data members
Reservation::Reservation( string theReservationNumber, string theTrainNumber,
                          string theIdNumber, string thePhone,
                          string theDate, int theOriginStation,
                          int theDestinationStation, int theCarClass,
                          int theAdultTickets , int theConcessionTickets )
{
   setReservationNumber( theReservationNumber );
   setTrainNumber( theTrainNumber );
   setIdNumber( theIdNumber );
   setPhone( thePhone );
   setDate( theDate );
   setOriginStation( theOriginStation );
   setDestinationStation( theDestinationStation );
   setCarClass( theCarClass );
   setAdultTickets( theAdultTickets );
   setConcessionTickets( theConcessionTickets );
} // end Reservation default constructor

void Reservation::setReservationNumber( string theReservationNumber )
{
   int length = theReservationNumber.size();
   length = ( length < 12 ? length : 11 );
   theReservationNumber.copy( reservationNumber, length );
   reservationNumber[ length ] = '\0';
}

string Reservation::getReservationNumber()
{
   string buffer( reservationNumber );
   return buffer;
}

void Reservation::setTrainNumber( string theTrainNumber )
{
   int length = theTrainNumber.size();
   length = ( length < 8 ? length : 7 );
   theTrainNumber.copy( trainNumber, length );
   trainNumber[ length ] = '\0';
}

string Reservation::getTrainNumber()
{
   string buffer( trainNumber );
   return buffer;
}

void Reservation::setIdNumber( string theIdNumber )
{
   int length = theIdNumber.size();
   length = ( length < 12 ? length : 11 );
   theIdNumber.copy( idNumber, length );
   idNumber[ length ] = '\0';
}

string Reservation::getIdNumber()
{
   string buffer( idNumber );
   return buffer;
}

void Reservation::setPhone( string thePhone )
{
   int length = thePhone.size();
   length = ( length < 12 ? length : 11 );
   thePhone.copy( phone, length );
   phone[ length ] = '\0';
}

string Reservation::getPhone()
{
   string buffer( phone );
   return buffer;
}

void Reservation::setDate( string theDate )
{
   int length = theDate.size();
   length = ( length < 12 ? length : 11 );
   theDate.copy( date, length );
   date[ length ] = '\0';
}

string Reservation::getDate()
{
   string buffer( date );
   return buffer;
}

void Reservation::setOriginStation( int theOriginStation )
{
   originStation = theOriginStation;
}

int Reservation::getOriginStation()
{
    return originStation;
}

void Reservation::setDestinationStation( int theDestinationStation )
{
   destinationStation = theDestinationStation;
}

int Reservation::getDestinationStation()
{
   return destinationStation;
}

void Reservation::setCarClass( int theCarClass )
{
   carClass = theCarClass;
}

int Reservation::getCarClass()
{
   return carClass;
}

void Reservation::setAdultTickets( int theAdultTickets )
{
   adultTickets = theAdultTickets;
}

int Reservation::getAdultTickets()
{
   return adultTickets;
}

void Reservation::setConcessionTickets( int theConcessionTickets )
{
   concessionTickets = theConcessionTickets;
}

int Reservation::getConcessionTickets()
{
   return concessionTickets;
}