// Train.cpp
// Member-function definitions for class Train.
#include <iostream>
#include <iomanip>
#include <cstring>
#include "Train.h" // Train class definition
using namespace std;

// Train default constructor initializes data members
Train::Train( string theTrainNumber, string *theDepartureTimes )
{
   setTrainNumber( theTrainNumber );
   setDepartureTimes( theDepartureTimes );
} // end Train default constructor

void Train::setTrainNumber( string theTrainNumber )
{
   int length = theTrainNumber.size();
   length = ( length < 8 ? length : 7 );
   theTrainNumber.copy( trainNumber, length );
   trainNumber[ length ] = '\0';
}

string Train::getTrainNumber()
{
   return trainNumber;
}

void Train::setDepartureTimes( string *theDepartureTimes )
{
   // important! if this case is not considered, there will be many runtime errors
   if( theDepartureTimes == NULL )
      for( int i = 0; i < 8; i++ )
      {
         strcpy( departureTimes[i], "" );
      }
   else
      for( int i = 0; i < 8; i++ )
      {
         int length = theDepartureTimes[i].size();
         length = ( length < 8 ? length : 7 );
         theDepartureTimes[i].copy( departureTimes[i], length );
         departureTimes[i][ length ] = '\0';
      }
}

string* Train::getDepartureTimes( string *buffer )
{
   for( int i = 0; i < 8; i++ )
      buffer[i].assign( departureTimes[i] );

   return buffer;
}

void Train::display()
{
   cout << setw(8) << trainNumber;
   for( int i = 0; i < 8; i++ )
      cout << setw(8) << departureTimes[i];
   cout << endl;
}