// Train.h
// Train class definition. Represents a train.
#ifndef TRAIN_H
#define TRAIN_H

#include <string>
using namespace std;

class Train
{
public:
   Train( string = "", string * = NULL ); // constructor initializes data members
   void setTrainNumber( string );
   string getTrainNumber();
   void setDepartureTimes( string * );
   string * getDepartureTimes( string * ); // returns the departure times of a train for all stations
   void display(); // display the train number and all departure times of a train
private:
   char trainNumber[ 8 ]; // used to identify a train
   char departureTimes[ 8 ][ 8 ]; // the departure time of a train for each station
};

#endif