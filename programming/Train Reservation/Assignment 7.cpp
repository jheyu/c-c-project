#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;
#include <vector>
#include <ctime>
#include "Reservation.h" // Reservation class definition
#include "Train.h" // Train class definition

int adultTicketPrice[8][8] = {    0,  260,  440,  640, 1250, 1820, 2230, 2440,
                                 40,    0,  400,  590, 1210, 1780, 2180, 2390,
                                160,  130,    0,  400, 1010, 1100, 1990, 2200,
                                290,  260,  130,    0,  820, 1390, 1790, 2000,
                                700,  670,  540,  410,    0,  770, 1180, 1390,
                               1080, 1050,  920,  790,  380,    0,  620,  820,
                               1350, 1320, 1190, 1060,  650,  280,    0,  410,
                               1490, 1460, 1330, 1200,  790,  410,  140,    0 };

int concessionTicketPrice[8][8] = {   0, 130, 220, 320, 625, 910, 1115, 1220,
                                     20,   0, 220, 295, 605, 890, 1090, 1195,
                                     80,  65,   0, 220, 505, 790,  995, 1100,
                                    145, 130,  65,   0, 410, 695,  895, 1000,
                                    350, 335, 270, 205,   0, 385,  590,  695,
                                    540, 525, 460, 395, 190,   0,  310,  410,
                                    675, 660, 595, 530, 325, 140,    0,  205,
                                    745, 730, 665, 600, 395, 205,   70,    0 };


char departureTimes[36][8] = { "06:00", "06:30", "07:00", "07:30", "08:00", "08:30", "09:00", "09:30", "10:00", "10:30",
                               "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", "15:00", "15:30",
                               "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00", "20:30",
                               "21:00", "21:30", "22:00", "22:30", "23:00", "23:30" };

int enterChoice();
void makingReservation( vector< Train > southboundTimetable, vector< Train > northboundTimetable );
void enterReservationDetails( Reservation &reservation, int &departureTime );
void loadSouthboundTimetable( vector< Train > &southboundTimetable );
void loadNorthboundTimetable( vector< Train > &northboundTimetable );
void southboundTrainSelection( vector< Train > southboundTimetable, Reservation &reservation, int departureTime );
void northboundTrainSelection( vector< Train > northboundTimetable, Reservation &reservation, int departureTime );
void enterPassengerInfor( Reservation &reservation );
void saveReservationDetails( Reservation reservation );
void reservationHistory( vector< Train > southboundTimetable, vector< Train > northboundTimetable );
bool hasReservation( fstream &ioFile, Reservation &reservation );
void displayReservationDetails( vector< Train > southboundTimetable, vector< Train > northboundTimetable, Reservation reservation );
void display( Reservation reservation, vector< Train > trainTimetable, char stations[8][12] );
int enterChoice1();
void reduceSeats( fstream &ioFile, vector< Train > southboundTimetable, vector< Train > northboundTimetable, Reservation &reservation );

int main()
{
   cout << "Taiwan High Speed Rail Booking System\n";
	srand( static_cast< unsigned int >( time(0) ) );

   vector< Train > southboundTimetable;
   vector< Train > northboundTimetable;
   int choice; // store user choice

   // enable user to specify action
   while ( ( choice = enterChoice() ) != 3 ) 
   {
      switch ( choice ) 
      {
         case 1:
				makingReservation( southboundTimetable, northboundTimetable );
            break;
         case 2:
            reservationHistory( southboundTimetable, northboundTimetable );
            break;
         default: // display error if user does not select valid choice
            cerr << "Incorrect Choice" << endl;
            break;
      } // end switch
   } // end while

   system( "pause" );
} // end main

// enable user to input menu choice
int enterChoice()
{
   // display available options
   cout << "\nEnter Your Choice\n"
      << "1. Booking\n"
      << "2. Booking History\n"
      << "3. End Program\n? ";

   int menuChoice = 1;
   cin >> menuChoice; // input menu selection from user
   return menuChoice;
} // end function enterChoice