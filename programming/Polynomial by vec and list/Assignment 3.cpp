#include <iostream>
using namespace std;
#include <ctime>
#include "Polynomial.cpp"

template< typename T >
void TestPolynomial( Polynomial< T > &a, Polynomial< T > &b );

int main()
{
//   srand( static_cast< unsigned int >( time(0) ) );
   unsigned int seed = 1;
   system("color f0");
   cout << "Enter seed: ";
   cin >> seed;
   cout << endl;

   srand( seed );
   Polynomial< Vector< Term > > a; // create an empty polynomial
   Polynomial< Vector< Term > > b; // create an empty polynomial

   TestPolynomial( a, b );

   cout << "************************************************************************************************\n\n";

   srand( seed );
   Polynomial< LinkedList< Term > > c; // create an empty polynomial
   Polynomial< LinkedList< Term > > d; // create an empty polynomial

   TestPolynomial( c, d );

   system( "pause" );
}

template< typename T >
void TestPolynomial( Polynomial< T > &a, Polynomial< T > &b )
{
   a.create( 4 ); // create a polynomial with 4 terms
   b.create( 3 ); // create a polynomial with 3 terms

   cout << "The first polynomial: " << a << endl;
   cout << "The second polynomial: " << b << endl;

   int x = rand() % 20;
   cout << "a(" << x << ") = " << a.evaluation( x ) << endl << endl;

   cout << "The sum: " << a + b << endl;

   cout << "The product: " << a * b << endl;
}