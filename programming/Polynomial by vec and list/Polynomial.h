#include "LinkedList.cpp" // include member-function definitions for class LinkedList
#include "Vector.cpp"

#ifndef POLYNOMIAL_H
#define POLYNOMIAL_H

// Represents a term of a polynomial
struct Term
{
   int coef;
   int expon;
};

// Polynomial class definition
template< typename T >
class Polynomial
{
   // Overloaded stream insertion operator
   template< typename U >
   friend ostream &operator<<( ostream &output, const Polynomial< U > &a );
public:
   Polynomial(); // Constructs an empty polynomial, with no terms.

   // Constructs a polynomial with a copy of each of the terms in polynomialToCopy.
   Polynomial( const Polynomial< T > &polynomialToCopy );

   ~Polynomial(); // Destructor with empty body

   // Copies all the terms from "right" into the polynomial
   const Polynomial< T > operator=( const Polynomial< T > &right );

   void create( int numTerms ); // Randomly generates a polynomial with "numTerms" terms

   long long int evaluation( const int x ); // Returns the value of a polynomial evaluated at x

   Polynomial< T > operator+( const Polynomial< T > &b ); // Returns the sum of two polynomials

   Polynomial< T > operator*( const Polynomial< T > &b ); // Returns the product of two polynomials
private:
   T polynomial; // a polynomial

   bool duplicate( Term *term, int i ); // Returns whether term[i].expon == term[j].expon for some j in [0, ..., i-1]

   void bubbleSort( Term *term, int numTerms ); // Sorts elements in the array "term" according to their expons

   // Returns 0 if x == y; returns -1 if x < y; and returns 1 if x > y.
   int compare( const int x, const int y );

   void attach( int coefficient, int exponent ); // Attaches a new term to the polynomial

   long long int power( const int base, const int expon ); // Returns base raised to the power of expon

   bool zero() const; // Returns true if and only if polynomial is a zero polynomial
}; // end class Polynomial

#endif

