// Member-function definitions for class ListIterator.
#include <iostream>
#include <iomanip>
using namespace std;
#include "ListIterator.h" // include definition of class ListIterator

// default constructor
template< typename T >
ListIterator< T >::ListIterator( ListNode< T > *p )
   : ptr( p )
{
}

// destructor
template< typename T >
ListIterator< T >::~ListIterator()
{
}

// overloaded dereference operator
template< typename T >
T& ListIterator< T >::operator*() const
{
   return ptr->data;
}

// overloaded class member access operator
template< typename T >
T* ListIterator< T >::operator->() const
{
   return &(ptr->data);
}
template<typename T>
ListIterator< T >& ListIterator< T >::operator++()
{
	ptr=ptr->next;
	return ptr;
}
template<typename T>
ListIterator< T > ListIterator< T >::operator++( int )
{
	ListNode< T > *temp;
	temp=ptr;
	ptr=ptr->next;
	return temp;
}
template<typename T>
bool ListIterator< T >::operator==( const ListIterator< T > &right ) const
{
	return ptr== right.ptr;
}
template<typename T>
bool ListIterator< T >::operator!=( const ListIterator< T > &right ) const
{
	return ptr!= right.ptr;//他是直接要比兩個class*******************************
}