#include <iostream>
using namespace std;
#include "ListNode.h" // include definition of class ListNode from ListNode.h

// ListIterator class definition.
#ifndef LIST_ITERATOR_H
#define LIST_ITERATOR_H

// ListIterator class definition
template< typename T >
class ListIterator
{
   template< typename U > friend class LinkedList;
public:
   ListIterator( ListNode< T > *p = 0 ); // default constructor
   ~ListIterator(); // destructor
   T& operator*() const; // dereferencing operator
   T* operator->() const; // class member access operator
   ListIterator< T >& operator++(); // prefix increment operator
   ListIterator< T > operator++( int ); // postfix increment operator
   bool operator==( const ListIterator< T > &right ) const; // equal to
   bool operator!=( const ListIterator< T > &right ) const; // not equal to
private:
   ListNode< T > *ptr; // keep a pointer to LinkedList
}; // end class ListIterator


#endif