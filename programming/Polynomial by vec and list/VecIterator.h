#include <iostream>
using namespace std;

#ifndef VEC_ITERATOR_H
#define VEC_ITERATOR_H

// VecIterator class definition.
template< typename T >
class VecIterator 
{
public:
   VecIterator( T *p = 0 ); // default constructor
   T& operator*() const; // dereferencing operator
   T* operator->() const; // class member access operator
   VecIterator< T >& operator++(); // prefix increment operator
   VecIterator< T > operator++( int ); // postfix increment operator
   bool operator==( const VecIterator< T > &right ) const; // equal to
   bool operator!=( const VecIterator< T > &right ) const; // not equal to
private:
   T *ptr; // keep a pointer to Vector
}; // end class VecIterator

#endif