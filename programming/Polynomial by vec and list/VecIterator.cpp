// Member-function definitions for class VecIterator.
#include <iostream>
#include <iomanip>
using namespace std;
#include "VecIterator.h" // include definition of class VecIterator

// default constructor
template< typename T >
VecIterator< T >::VecIterator( T *p )
   : ptr( p )
{
}

// overloaded dereference operator
template< typename T >
T& VecIterator< T >::operator*() const
{
   return *ptr;
}

// overloaded class member access operator
template< typename T >
T* VecIterator< T >::operator->() const
{
   return ptr;
}
template< typename T >
VecIterator< T >& VecIterator< T >::operator++()//先++
{
	ptr++;
	return *this;//THIS裡的地址
}
template< typename T >
VecIterator< T > VecIterator< T >::operator++( int )//後++
{
	VecIterator< T >temp(*this);//要傳她�媕Y的地址進去
	ptr++;
	return temp;
}
template< typename T >
bool VecIterator< T >::operator==( const VecIterator< T > &right ) const
{
	return right.ptr==ptr;
}
template< typename T >
bool VecIterator< T >::operator!=( const VecIterator< T > &right ) const
{
	return right.ptr!=ptr;
}