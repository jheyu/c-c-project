#include <iostream>
#include <iomanip>
using namespace std;
#include "ListIterator.cpp"
#include "Polynomial.h"

const int MAX_VALUE = 10;

// Constructs an empty polynomial, with no terms.
template< typename T >
Polynomial< T >::Polynomial()
   : polynomial()
{
}

// Constructs a polynomial with a copy of each of the terms in polynomialToCopy.
template< typename T >
Polynomial< T >::Polynomial( const Polynomial &polynomialToCopy )
   : polynomial( polynomialToCopy.polynomial )
{
}

// Destructor with empty body
template< typename T >
Polynomial< T >::~Polynomial()
{
}

// Copies all the terms from "right" into the polynomial
template< typename T >
const Polynomial< T > Polynomial< T >::operator=( const Polynomial< T > &right )
{
   if ( &right != this ) // avoid self-assignment
      polynomial = right.polynomial;

   return *this; // enables x = y = z, for example
}

// Randomly generates a polynomial with "numTerms" terms
template< typename T >
long long int Polynomial< T >::evaluation( const int x )
{
	T::iterator ptr1 = polynomial.begin();
	long long int answer = 0;
	for(;ptr1!=polynomial.end();ptr1++)
		answer += ptr1->coef*pow(x,ptr1->expon);
	return answer;
	
}
template< typename T >
Polynomial< T > Polynomial< T >::operator+( const Polynomial< T > &b )
{
	Polynomial< T > newPolynomial;
	T::iterator ptr1 = polynomial.begin();
	T::iterator ptr2 = b.polynomial.begin();
	int expon = 0, coef = 0;
	while(ptr1!=polynomial.end() && ptr2 != b.polynomial.end()){
		switch(compare(ptr1->expon,ptr2->expon))
		{
			case -1://ptr1.expon小於ptr2.expon
				coef=ptr2->coef;
				expon=ptr2->expon;
				newPolynomial.attach( coef, expon );
				ptr2++;
				break;
			case 0://等於
				coef=ptr1->coef+ptr2->coef;
				expon=ptr1->expon;
				if (coef) newPolynomial.attach( coef, expon );
				ptr1++,ptr2++;
				break;
			case 1://大於
				coef=ptr1->coef;
				expon=ptr1->expon;
				newPolynomial.attach( coef, expon );
				ptr1++;	
		}
	}
	for(;ptr1!=polynomial.end();ptr1++){	
		coef=ptr1->coef;
		expon=ptr1->expon;
		newPolynomial.attach( coef, expon );
	}
	for(;ptr2!=b.polynomial.end();ptr2++){	
		coef=ptr2->coef;
		expon=ptr2->expon;
		newPolynomial.attach( coef, expon );
	}
	return newPolynomial;

}
template< typename T >
Polynomial< T > Polynomial< T >::operator*( const Polynomial< T > &b )
{
	
	T::iterator ptr1 = polynomial.begin();
	T::iterator ptr2 = b.polynomial.begin();
	int term1Number = 0;
	int termsNumber = 0;
	int expon = 0, coef = 0;
	for(;ptr1!=polynomial.end();ptr1++)
		term1Number++;
	Polynomial< T > *newPolynomial=new Polynomial< T >[term1Number++];
	for(int i = 1;ptr2!=b.polynomial.end();ptr2++,i++){
		for(ptr1 = polynomial.begin();ptr1!=polynomial.end();ptr1++){
			expon=ptr2->expon+ptr1->expon;
			coef=ptr2->coef*ptr1->coef;
			newPolynomial[i].attach( coef, expon );
		}
	}
	for(int i =1;i<term1Number;i++)
		newPolynomial[0]=newPolynomial[0]+newPolynomial[i];
	for(T::iterator ptr3 = newPolynomial[0].polynomial.begin();ptr3!=newPolynomial[0].polynomial.end();ptr3++)
		termsNumber++;
	bubbleSort( &(*newPolynomial[0].polynomial.begin()), termsNumber );
   return newPolynomial[0];
}
template< typename T >
void Polynomial< T >::create( int numTerms )
{
   Term *term = new Term[ numTerms ];

   for( int i = 0; i < numTerms; i++ )
      do {
         term[i].coef = 1 + rand() % MAX_VALUE;
         term[i].expon = 1 + rand() % MAX_VALUE;         
      } while( duplicate( term, i ) );

   bubbleSort( term, numTerms );

   for( int i = 0; i < numTerms; i++ )
      polynomial.push_back( term[i] );
}

// Returns whether term[i].expon == term[j].expon for some j in [0, ..., i-1]
template< typename T >
bool Polynomial< T >::duplicate( Term *term, int i )
{
   for( int j = 0; j < i; j++ )
       if( term[i].expon == term[j].expon )
          return true;

   return false;
}
// Sorts elements in the array "term" according to their expons
template< typename T >
void Polynomial< T >::bubbleSort( Term *term, int numTerms )
{
   Term hold;

   for ( int pass = numTerms - 1; pass > 0; pass-- )
      for ( int i = 0; i < pass; i++ )  
         if ( term[ i ].expon < term[ i + 1 ].expon )
         {
            hold = term[ i ];
            term[ i ] = term[ i + 1 ];
            term[ i + 1 ] = hold;
         }
}

// Returns true if and only if polynomial is a zero polynomial
template< typename T >
bool Polynomial< T >::zero() const
{
   return polynomial.empty();
}

// Returns 0 if x == y; returns -1 if x < y; and returns 1 if x > y.
template< typename T >
int Polynomial< T >::compare( const int x, const int y )
{
   if( x < y )
      return -1;
   else if( x == y )
      return 0;
   else
      return 1;
}

// Attaches a new term to the polynomial
template< typename T >
void Polynomial< T >::attach( int coefficient, int exponent )
{
   Term tempTerm;
   tempTerm.coef = coefficient;
   tempTerm.expon = exponent;
   polynomial.push_back( tempTerm );
}

// Returns base raised to the power of expon
template< typename T >
long long int Polynomial< T >::power( const int base, const int expon )
{
   long long int result = 1;
   for( int i = 0; i < expon; i++ )
      result *= base;

   return result;
}

// Overloaded stream insertion operator
template< typename T >
ostream &operator<<( ostream &output, const Polynomial< T > &a )
{
   if( a.zero() )
   {
      output << 0 << endl;
      return output;
   }

   T::iterator it = a.polynomial.begin();

	output << it->coef;
   if( it->expon != 0 )
      output << "x^" << it->expon;

   for( it++; it != a.polynomial.end(); it++ )
   {
      if( it->coef > 0 )
         output << " + " << it->coef;
      else
         output << " - " << -it->coef;

      if( it->expon != 0 )
         output << "x^" << it->expon;
   }

   output << endl;

   return output; // enables cout << x << y;
} // end function operator<<

