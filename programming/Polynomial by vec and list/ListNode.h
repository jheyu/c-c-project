#ifndef LIST_NODE_H
#define LIST_NODE_H

template< typename T > class LinkedList;   // forward declare
template< typename T > class ListIterator; // forward declare

// ListNode class definition
template< typename T >
class ListNode
{
   template< typename U > friend class LinkedList;
   template< typename U > friend class ListIterator;
private:
   T data;
   ListNode< T > *next;
}; // end class ListNode

#endif