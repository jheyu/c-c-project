#include "ListIterator.h" // include definition of class ListIterator

#ifndef LINKED_LIST_H
#define LINKED_LIST_H

// LinkedList class definition
template< typename T >
class LinkedList
{
public:
   typedef ListIterator< T > iterator;
   LinkedList(); // Constructs an empty linked list, with no elements.

   // Constructs a linked list with a copy of each of the elements in listToCopy, in the same order.
   LinkedList( const LinkedList< T > &listToCopy );

   // Destroys all linked list elements, and deallocates all the storage allocated by the linked list.
   ~LinkedList();

   // Copies all the elements from "right" into the linked list 
   const LinkedList< T > &operator=( const LinkedList< T > &right );

   const ListIterator< T > begin() const; // Returns head

   const ListIterator< T > end() const; // Returns NULL

   // Inserts a new element at the end of the linked list, right after its current last element.
   void push_back( const T val );

   //Returns a bool value indicating whether the linked list is empty, i.e. whether head == NULL.
   bool empty() const;

   // Removes all elements from the linked list (which are destroyed),
   void clear(); // and resets head and tail to null pointer
private:
   ListNode< T > *head; // pointer to the first element of linked list
   ListNode< T > *tail; // pointer to the last element of linked list
}; // end class LinkedList

#endif