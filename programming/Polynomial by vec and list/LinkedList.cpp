// Member-function definitions for class LinkedList.

#include <iostream>
using namespace std;
#include "LinkedList.h" // include definition of class LinkedList from LinkedList.h

// Member-function definitions for class LinkedList.
#ifndef LINKED_LIST_CPP
#define LINKED_LIST_CPP

// Constructs an empty linked list, with no elements.
template< typename T >
LinkedList< T >::LinkedList()
{
   head = NULL;
   tail = NULL;
}

// Destroys all linked list elements, and deallocates all the storage allocated by the linked list.
template< typename T >
LinkedList< T >::~LinkedList()
{
   clear();
}
template <typename T >
LinkedList< T >::LinkedList( const LinkedList< T > &listToCopy )
{
	head = listToCopy.head;
	tail = listToCopy.tail;
	//頭尾相接!?
}
template<typename T>
const LinkedList< T >  & LinkedList<T>::operator=( const LinkedList< T > &right )
{
	head = right.head;
	tail = right.tail;
	return *this;
}
template< typename T >
const ListIterator< T > LinkedList< T >::begin() const
{
   return ListIterator< T >( head );
}

template< typename T >
const ListIterator< T > LinkedList< T >::end() const
{
   return ListIterator< T >( NULL );
}
template<typename T>
void LinkedList<T>:: push_back( const T val )
{
	ListNode< T > pushIn;
	pushIn.data=val;
	pushIn.next=0;
	tail->next=&pushIn;
	tail=&pushIn;

	if(empty())
		head->next=&pushIn;

}
template<typename T>
void LinkedList<T>::clear()
{

	for(iterator ptr1 = head->next;ptr1!=tail->next;ptr1++){
		free(head->next);
		head->next= ptr1.ptr;//ITERATOR裡的ptr才是存放head->next的地址**************************
	}
	free(tail->next);
}
//Returns a bool value indicating whether the linked list is empty.
template< typename T >
bool LinkedList< T >::empty() const
{
   return ( head == NULL );
}

#endif