#include "VecIterator.h" // include definition of class VecIterator
// Vector class definition.

#ifndef VECTOR_H
#define VECTOR_H

template< typename T >
class Vector
{
public:
   typedef VecIterator< T > iterator;

   Vector( unsigned int n = 0 ); // Constructs a vector with n elements. Each element is a copy of val.
   Vector( const Vector< T > &vectorToCopy ); // Constructs a vector with a copy of each of the elements in x, in the same order.
   ~Vector(); // Destroys the vector.

   const Vector< T > &operator=( const Vector< T > &right ); // assignment operator

   const VecIterator< T > begin() const; // Returns a pointer pointing to the first element in the vector.
                                         // If the vector is empty, the returned pointer shall not be dereferenced.

   const VecIterator< T > end() const; // Returns an pointer referring to the past-the-end element in the vector.
               // The past-the-end element is the theoretical element that would follow the last element in the vector.
               // It does not point to any element, and thus shall not be dereferenced.

   void push_back( const T val ); // Adds a new element at the end of the vector, after its current last element.
                                  // The content of val is copied to the new element.
                                  // This effectively increases the container size by one,
                                  // which causes an automatic reallocation of the allocated storage space
                                  // if -and only if- the new vector size surpasses the current vector capacity.

   bool empty() const; // Returns whether the vector is empty (i.e. whether its size is 0).
private:
   unsigned int size; // the number of elements in the vector
                      // This is the number of actual objects held in the vector, which is not necessarily equal to its storage capacity.
   unsigned int capacity; // the size of the storage space currently allocated for the vector, expressed in terms of elements.
                          // This capacity is not necessarily equal to the vector size. It can be equal or greater,
                          // with the extra space allowing to accommodate for growth without the need to reallocate on each insertion.
   T *ptr; // points to a dynamically allocated array which is used to store the elements of the vector
}; // end class Vector

#endif