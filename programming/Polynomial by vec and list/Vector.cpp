#include <iostream>
#include <iomanip>
using namespace std;
#include "Vector.h" // include definition of class Vector
#include "VecIterator.cpp"

// Member-function definitions for class Vector.
#ifndef VECTOR_CPP
#define VECTOR_CPP

 // Constructs a vector with n elements. Each element is a copy of val
template< typename T >
Vector< T >::Vector( unsigned int n )
{
   ptr = new T[ n ];

   size = n;
   capacity = n;
} // end Vector constructor
template< typename T >
Vector< T >::Vector( const Vector< T > &vectorToCopy )
{
	size=vectorToCopy.size;
	capacity=vectorToCopy.capacity;
	ptr= new T [size];
	for(iterator ptr1=vectorToCopy.begin(),ptr2 = begin();ptr1!=vectorToCopy.end();ptr1++,ptr2++)
		*ptr2=*ptr1;
}
// destructor
template< typename T >
Vector< T >::~Vector()
{
   delete [] ptr;
} // end destructor
template< typename T >
const Vector< T > &Vector< T >::operator=( const Vector< T > &right )
{
	delete[] ptr;
	size=right.size;
	this->capacity=right.capacity;
	ptr= new T [size];
	for(iterator ptr1=right.begin(),ptr2 = begin();ptr1!=right.end();ptr1++,ptr2++)
		*ptr2=*ptr1;
	return *this;//要回傳THIS中所放的地址所以要用*
}
template< typename T >
const VecIterator< T > Vector< T >::begin() const
{
   return VecIterator< T >( ptr );
}
template< typename T >
const VecIterator< T > Vector< T >::end() const
{
   return VecIterator< T >( ptr + size );
}
template< typename T >
void Vector< T >::push_back( const T val )
{
	Vector< T >buffer(*this);
	if(!empty())delete[]ptr;
	size +=1;
	capacity +=1;
	ptr = new T[size];
	iterator ptr2=begin();
	for(iterator ptr1=buffer.begin();ptr1!=buffer.end();ptr1++,ptr2++)
		*ptr2=*ptr1;
	*(ptr2)=val;
}
template< typename T >
bool Vector< T >::empty() const
{
   return ( size == 0 );
}

#endif